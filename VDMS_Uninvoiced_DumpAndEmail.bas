Attribute VB_Name = "VDMS_Invoice_Posting"
Option Explicit
Dim g_strConnection As String

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Content Delivery at MX1 London"
Const g_strFullUSerName = "MX1 London Operations"
Const g_strUploadInvoiceLocation = "\\jcaserver\ceta\Invoices\"

Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strBookingsEmailAddress As String
Public g_strBookingsEmailName As String
Public g_strVDMSEmailForInvoices As String
Public g_optCostDubbing As Integer
Public g_optCostSchedule As Integer
Public g_optUseProductRateCards As Integer
Public g_intBaseRateCard As Integer
Public g_intDontCostScheduledResourcesIfJobTypeIsADub As Integer
Public g_strRateCardType As String
Public g_optAlwaysUseRateCardDescriptions As Integer
Public g_optDeductPreSetDiscountsFromUnitPrice As Integer
Public g_optAbortIfAZeroDuration As Integer
Public g_optAddDurationToRateCode As Integer
Public g_optDontMakeMachineTimeADecimal As Integer
Public g_optUseAdditionalDescriptionsWhenCosting As Integer
Public g_optUseMasterDetailOnInvoice As Integer
Public g_optCostDespatch As Integer
Public g_optHideTimeColumn As Integer
Public g_intDefaultTimeWhenZero As Integer
Public g_optRoundCurrencyDecimals As Integer

Public Const g_strBatchPrefix = "J"

Public cnn As ADODB.Connection
Public l_strCommandLine As String

Public g_intWebInvoicing As Integer
Public g_intPostToPriority As Integer

Public m_blnPostingToAccounts As Boolean

'used when checking if job is safe to send to accounts
Enum JobSafeForPostingLevel
    vbuJobSafe = -1
    vbuJobNotSafeCostingRows = 2
    vbuJobNotSafeCompanyAccountCode = 3
    vbuJobNotSafeNoPONumber = 4
    vbuJobNotSafePOTooLong = 5
    vbuJobNotSafeSAPcode = 6
End Enum

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long

Private Declare Function WNetGetConnection Lib _
   "mpr.dll" Alias "WNetGetConnectionA" (ByVal lpszLocalName _
   As String, ByVal lpszRemoteName As String, _
   cbRemoteName As Long) As Long

Private Const MAX_COMPUTERNAME_LENGTH As Long = 15&

' Reg Key ROOT Types...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Unicode nul terminated string
Const REG_DWORD = 4                      ' 32-bit number

Private Declare Function RegOpenKey Lib "advapi32.dll" _
  Alias "RegOpenKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long
   
Private Declare Function RegEnumValue Lib "advapi32.dll" _
   Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex _
   As Long, ByVal lpValueName As String, lpcbValueName _
   As Long, ByVal lpReserved As Long, lpType As Long, _
   ByVal lpData As String, lpcbData As Long) As Long
   
Private Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hKey As Long) As Long
  
Sub Main()

Dim l_strWorkstationprops As String

Set cnn = New ADODB.Connection

l_strCommandLine = Command()
If InStr(1, l_strCommandLine, "/testSystem", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=VIE-TEST-sql-1;Failover_Partner=VIE-TEST-sql-2;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
ElseIf InStr(1, l_strCommandLine, "/testTim", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER=SQL Server;SERVER=VIE-Z820-TIM.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
Else
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=vie-sql-1.jcatv.co.uk;Failover_Partner=vie-sql-2.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'    g_strConnection = "DRIVER=SQL Server;SERVER=vie-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
End If

cnn.Open g_strConnection

If InStr(l_strCommandLine, "/monthly") > 0 Then
    'Test if it is the first working day of the month
    If Format(Now, "YYYY-MM-DD") <> Format(GetDataSQL("select top 1 fulldate from Holiday_Dates where Period = cast(year(getdate()) as nvarchar(4)) + format(month(getdate()),'00') and DayDescription = 'Working Day' order by FullDate asc;"), "YYYY-MM-DD") Then
        cnn.Close
        Set cnn = Nothing
        Exit Sub
    End If
End If

l_strWorkstationprops = GetData("xref", "descriptionalias", "description", CurrentMachineName)

'Set up the default email varaibles for use with notification emails
SetDefaultParameters
App.LogEvent "MX1 Weekly UnInvoiced Items Posting Started"

Dim l_strSQL As String, l_rstJobs As ADODB.Recordset, l_rstCostings As ADODB.Recordset

l_strSQL = "SELECT jobID FROM job WHERE completeddate IS NULL AND (flagquote IS NULL or flagquote = 0) AND createddate > '01-01-" & Year(Now) & "'"
l_strSQL = l_strSQL & "AND companyID > 100 ORDER BY jobID;"
Set l_rstJobs = New ADODB.Recordset
Set l_rstCostings = New ADODB.Recordset
l_rstJobs.Open l_strSQL, cnn, 3, 3
If l_rstJobs.RecordCount > 0 Then
    l_rstJobs.MoveFirst
    Do While Not l_rstJobs.EOF
        l_strSQL = "SELECT count(costingID) FROM costing WHERE jobID = " & l_rstJobs("jobID") & ";"
        l_rstCostings.Open l_strSQL, cnn, 3, 3
        If l_rstCostings(0) = 0 Then
            'MsgBox "Calculating Costing for Job " & l_rstJobs("jobID"), vbInformation, "Costing Jobs"
            CostJob l_rstJobs("jobID"), 0 - GetFlag(g_optCostDubbing), 0 - GetFlag(g_optCostSchedule), True
        End If
        l_rstCostings.Close
        l_rstJobs.MoveNext
    Loop
End If
l_rstJobs.Close
Set l_rstCostings = Nothing
Set l_rstJobs = Nothing

ExportCrystalReport "WorkList_NotInvoiced_all.rpt", "", g_strUploadInvoiceLocation & "WorkList_NotInvoiced_all_" & Format(Now, "YYYY-MM-DD") & ".xls"
NotifyByEMail g_strUploadInvoiceLocation & "WorkList_NotInvoiced_all_" & Format(Now, "YYYY-MM-DD") & ".xls"

cnn.Close

Set cnn = Nothing
Beep
End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetDataSQL(lp_strSQL As String) As Variant

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strConnection
l_rstGetData.Open lp_strSQL, l_conGetData, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function
Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub SetDefaultParameters()

Dim rst As New ADODB.Recordset, SQL As String

SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailName = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailName = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'VDMSEmailForInvoices';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strVDMSEmailForInvoices = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'CostDubbing';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optCostDubbing = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'CostSchedule';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optCostSchedule = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'UseProductRateCards';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optUseProductRateCards = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BaseRateCard';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_intBaseRateCard = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'DontCostScheduledResourcesIfJobTypeIsADub';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_intDontCostScheduledResourcesIfJobTypeIsADub = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'RateCardType';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strRateCardType = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AlwaysUseRateCardDescriptions';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optAlwaysUseRateCardDescriptions = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'DeductPreSetDiscountsFromUnitPrice';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optDeductPreSetDiscountsFromUnitPrice = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AbortIfAZeroDuration';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optAbortIfAZeroDuration = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AddDurationToRateCode';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optAddDurationToRateCode = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'DontMakeMachineTimeADecimal';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optDontMakeMachineTimeADecimal = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'UseAdditionalDescriptionsWhenCosting';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optUseAdditionalDescriptionsWhenCosting = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'UseMasterDetailOnInvoice';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optUseMasterDetailOnInvoice = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'CostDespatch';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optCostDespatch = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AlwaysHideTimeColumn';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optHideTimeColumn = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'DefaultTimeWhenZero';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_intDefaultTimeWhenZero = Val(Trim(" " & rst("value")))
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'RoundCurrencyDecimals';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_optRoundCurrencyDecimals = Val(Trim(" " & rst("value")))
End If
rst.Close

End Sub

Sub SelectAndPostInvoices()

Dim l_strWhereSQL As String, l_strSQL As String, rst As ADODB.Recordset, l_strType As String
Dim l_lngBatchNumber As Long, l_lngCreditBatchNumber As Long, l_lngCurrentJobID As Long, l_strNotPostMsg As String

On Error GoTo PROC_ERROR_INVOICE

l_strType = "INVOICE"

'invoices
l_strWhereSQL = " (job.invoiceddate Is Not null) and (job.jobtype <> 'CREDIT') AND (job.senttoaccountsdate Is null) AND (job.cancelleddate Is null) AND (job.flagquote <> " & GetFlag(1) & ")"
l_strSQL = "SELECT * FROM job WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
m_blnPostingToAccounts = True
    
Debug.Print l_strSQL
Set rst = New ADODB.Recordset
rst.Open l_strSQL, cnn, 3, 3

If rst.RecordCount > 0 Then
    rst.MoveFirst
    
    'get the next batch number
    l_lngBatchNumber = GetNextSequence("batchnumber")
    
    App.LogEvent "Commencing Posting Invoices"
    
    Do While Not rst.EOF
    
        'pick up the current job ID
        l_lngCurrentJobID = rst("jobID")
        l_strNotPostMsg = "The job with ID " & l_lngCurrentJobID & " can not be posted as "
        DoEvents
                
        'check to see if there are any problems with this jobs costing records
        Select Case JobSafeForPosting(l_lngCurrentJobID)
        Case vbuJobSafe
                
            If SendJobToAccounts(l_lngCurrentJobID, l_strType, l_lngBatchNumber) = True Then
            
                'update the job status, but use the parameter to stop the sent to accounts date being re-set
                SetData "job", "senttoaccountsdate", "jobID", l_lngCurrentJobID, FormatSQLDate(Now)
                SetData "job", "senttoaccountsuser", "jobID", l_lngCurrentJobID, "CETA"
                SetData "job", "fd_status", "jobid", l_lngCurrentJobID, "Sent To Accounts"
                cnn.Execute "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & l_lngCurrentJobID & "', 'CETA', '" & FormatSQLDate(Now) & "', 'Sent To Accounts', GETdate())"
            Else
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            End If
                            
        Case vbuJobNotSafeNoPONumber
            'no PO number on the invoice
            NotifyProblemByEmail l_lngCurrentJobID, "There is no PO number on the invoice"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeCompanyAccountCode
            'no company account code
            NotifyProblemByEmail l_lngCurrentJobID, "There is no company account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafeCostingRows
            'problem with costing lines
            NotifyProblemByEmail l_lngCurrentJobID, "There is at least one costing line without an account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafePOTooLong
            NotifyProblemByEmail l_lngCurrentJobID, "The PO number on the invoice has more than 100 characters"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeSAPcode
            NotifyProblemByEmail l_lngCurrentJobID, "There is no SAP code against the current customer"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        End Select
        
        'move to the next job
        rst.MoveNext
    
    Loop
    
    'Reports and closedown
    App.LogEvent "Printing Batch Report " & g_strBatchPrefix & l_lngBatchNumber & " to report batchreport.rpt"
    PrintCrystalReport "batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", False
    App.LogEvent "Exporting Batch Report " & g_strBatchPrefix & l_lngBatchNumber
    ExportCrystalReport "batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    ExportCrystalReport "batchsagereport.rpt", "{batchsage.Details} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    App.LogEvent "Notifying by Email " & g_strBatchPrefix & l_lngBatchNumber
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    
End If
rst.Close
Set rst = Nothing
App.LogEvent "Completed Posting Invoices"

PROC_EXIT:
Exit Sub

PROC_ERROR_INVOICE:
SendSMTPMail g_strAdministratorEmailAddress, "", "SelectAndPostInvoices generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Sub

Sub SelectAndPostCreditNotes()

Dim l_strWhereSQL As String, l_strSQL As String, rst As ADODB.Recordset, l_strType As String
Dim l_lngBatchNumber As Long, l_lngCreditBatchNumber As Long, l_lngCurrentJobID As Long, l_strNotPostMsg As String

On Error GoTo PROC_ERROR_CREDIT

l_strType = "CREDIT"

'credit notes
l_strWhereSQL = "(jobtype='CREDIT' AND job.creditnotedate IS NOT NULL AND creditsenttoaccountsdate IS NULL AND cancelleddate IS NULL AND flagquote <> " & GetFlag(1) & ")"
l_strSQL = "SELECT jobID, creditnotenumber as invoicenumber, creditsenttoaccountsdate as senttoaccountsdate, creditsenttoaccountsuser as senttoaccountsuser, companyname, creditnotedate as invoiceddate, deadlinedate, createddate, createduser, companyID, contactname, contactID, telephone, email, fax, orderreference, title1, title2, startdate FROM job WHERE " & l_strWhereSQL & " ORDER BY creditnotenumber, jobID"
m_blnPostingToAccounts = True

Set rst = New ADODB.Recordset
rst.Open l_strSQL, cnn, 3, 3

If rst.RecordCount > 0 Then
    rst.MoveFirst
        
    'get the next batch number
    l_lngBatchNumber = GetNextSequence("batchnumber")
    
    App.LogEvent "Commencing Posting Credit Notes"
    
    Do While Not rst.EOF
        
        'pick up the current job ID
        l_lngCurrentJobID = rst("jobID")
        l_strNotPostMsg = "The job with ID " & l_lngCurrentJobID & " can not be posted as "
        DoEvents
                
        'check to see if there are any problems with this jobs costing records
        Select Case JobSafeForPosting(l_lngCurrentJobID)
        Case vbuJobSafe
                
            If SendJobToAccounts(l_lngCurrentJobID, l_strType, l_lngBatchNumber) = True Then
            
                'update the job status, but use the parameter to stop the sent to accounts date being re-set
                SetData "job", "senttoaccountsdate", "jobID", l_lngCurrentJobID, FormatSQLDate(Now)
                SetData "job", "senttoaccountsuser", "jobID", l_lngCurrentJobID, "CETA"
                SetData "job", "fd_status", "jobid", l_lngCurrentJobID, "Sent To Accounts"
                cnn.Execute "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & l_lngCurrentJobID & "', 'CETA', '" & FormatSQLDate(Now) & "', 'Sent To Accounts', GETdate())"
            Else
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            End If
            
        Case vbuJobNotSafeNoPONumber
            'no PO number on the invoice
            NotifyProblemByEmail l_lngCurrentJobID, "There is no PO number on the invoice"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeCompanyAccountCode
            'no company account code
            NotifyProblemByEmail l_lngCurrentJobID, "There is no company account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafeCostingRows
            'problem with costing lines
            NotifyProblemByEmail l_lngCurrentJobID, "There is at least one costing line without an account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafePOTooLong
            NotifyProblemByEmail l_lngCurrentJobID, "The PO number on the invoice has more than 100 characters"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeSAPcode
            NotifyProblemByEmail l_lngCurrentJobID, "There is no SAP code against the current customer"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        End Select
        
        'move to the next job
        rst.MoveNext
    
    Loop
    
    'Reports and closedown
    App.LogEvent "Printing Batch Report " & g_strBatchPrefix & l_lngBatchNumber & " to report batchreport.rpt"
    PrintCrystalReport "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", False
    App.LogEvent "Exporting Batch Report " & g_strBatchPrefix & l_lngBatchNumber
    ExportCrystalReport "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    ExportCrystalReport "Batchsagereport.rpt", "{batchsage.Details} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    App.LogEvent "Notifying by Email " & g_strBatchPrefix & l_lngBatchNumber
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    
End If
rst.Close
Set rst = Nothing
App.LogEvent "Completed Posting Credit Notes"

PROC_EXIT:
Exit Sub

PROC_ERROR_CREDIT:
SendSMTPMail g_strAdministratorEmailAddress, "", "SelectAndPostCreditNotes generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Sub

Function PostNewCustomersToAccounts() As Boolean
    
Dim l_rstNewCustomers As New ADODB.Recordset
Dim l_strSQL As String, l_lngBatchNumber As Long
Dim cnn2 As ADODB.Connection, rst As ADODB.Recordset
Dim ACCNUMBER As String

l_strSQL = "SELECT * FROM company WHERE postedtoaccountsdate IS NULL AND iscustomer = 1 and system_active <> 0;"
l_rstNewCustomers.Open l_strSQL, cnn, 3, 3

l_lngBatchNumber = GetNextSequence("batchnumber")

'On Error GoTo PROC_ERROR_CUSTOMERS

If Not l_rstNewCustomers.EOF Then
    l_rstNewCustomers.MoveFirst
    While Not l_rstNewCustomers.EOF
        If Trim(" " & l_rstNewCustomers("salesledgercode")) = "" Then
            PostNewCustomersToAccounts = False
            l_rstNewCustomers.Close
            Set l_rstNewCustomers = Nothing
            Exit Function
        End If
        App.LogEvent "Posting New Customer " & l_rstNewCustomers("salesledgercode")
        ' Do the traditional posting anyway
        l_strSQL = "INSERT INTO batchcompany (batchnumber, code, clientname, address1, postcode, telephone, fax, vatnumber, ecvatstatus, eccountry, ecvat, ratecard, ready, clientnumber, BusinessUnit, email) VALUES ("
        l_strSQL = l_strSQL & l_lngBatchNumber & ", "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("salesledgercode") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("name")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("address")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("postcode") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("telephone") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("fax") & "', "
        Select Case l_rstNewCustomers("vatcode")
            Case "G"
                l_strSQL = l_strSQL & "'" & Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2) & "', "
                l_strSQL = l_strSQL & "'G', "
                l_strSQL = l_strSQL & "'" & Left(l_rstNewCustomers("vatnumber"), 2) & "', "
                l_strSQL = l_strSQL & "'E', "
            Case "7"
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'7', "
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'O', "
            Case Else
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'1', "
                l_strSQL = l_strSQL & "'GB', "
                l_strSQL = l_strSQL & "'U', "
        End Select
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("ratecard") & "', "
        l_strSQL = l_strSQL & "'READY', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("companyID") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("BusinessUnit") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("email") & "') "
        
        cnn.Execute l_strSQL
            
        If g_intPostToPriority <> 0 Then
        
            'Then also do the priority posting
            Set cnn2 = New ADODB.Connection
            cnn2.ConnectionString = "DRIVER=SQL Server;SERVER=172.19.4.232;DATABASE=rreur;UID=RReuropeSQL;PWD=Re1234"
            cnn2.Open
            
            Set rst = cnn2.Execute("SELECT ACCNAME FROM HSLOADACCOUNTS WHERE ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';")
            If rst.EOF Then
                rst.Close
                l_strSQL = "INSERT INTO HSLOADACCOUNTS (TYPE, DNAME, ACCNAME, ACCDES, ADDRESS, ZIP, PHONE, FAX, VATNUM, SPEC1, COUNTRYNAME, SPEC3, SPEC6, DETAILS, EMAIL) VALUES ('R', 'CETA', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("salesledgercode")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(QuoteSanitise(StrReverse(l_rstNewCustomers("name")))) & "', "
                l_strSQL = l_strSQL & "'" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("postcode")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("telephone")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("fax")) & "', "
                Select Case l_rstNewCustomers("vatcode")
                    Case "G"
                        l_strSQL = l_strSQL & "'" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                        l_strSQL = l_strSQL & "'000', "
                        l_strSQL = l_strSQL & "'" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                        l_strSQL = l_strSQL & "'E', "
                    Case "7"
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'800', "
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'O', "
                    Case Else
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'100', "
                        l_strSQL = l_strSQL & "'GB', "
                        l_strSQL = l_strSQL & "'U', "
                End Select
                Select Case Trim(" " & l_rstNewCustomers("Businessunit"))
                    Case "S&E"
                        l_strSQL = l_strSQL & "'" & StrReverse("OU") & "', "
                    Case "TVI"
                        l_strSQL = l_strSQL & "'" & StrReverse("24/7") & "', "
                    Case Else
                        l_strSQL = l_strSQL & "'" & StrReverse(Trim(" " & l_rstNewCustomers("Businessunit"))) & "', "
                End Select
                l_strSQL = l_strSQL & "'READY', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("email")) & "') "
                
            Else
                ACCNUMBER = rst("ACCNAME")
                l_strSQL = "UPDATE HSLOADACCOUNTS SET ACCNAME = '" & ACCNUMBER & "', ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "' WHERE ACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';"
                Debug.Print l_strSQL
                cnn.Execute l_strSQL
                rst.Close
                l_strSQL = "UPDATE HSLOADACCOUNTS SET "
                l_strSQL = l_strSQL & "ADDRESS = '" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                l_strSQL = l_strSQL & "ZIP = '" & Trim(QuoteSanitise(l_rstNewCustomers("postcode"))) & "', "
                l_strSQL = l_strSQL & "PHONE = '" & Trim(QuoteSanitise(l_rstNewCustomers("telephone"))) & "', "
                l_strSQL = l_strSQL & "FAX = '" & Trim(QuoteSanitise(l_rstNewCustomers("fax"))) & "', "
                Select Case l_rstNewCustomers("vatcode")
                    Case "G"
                        l_strSQL = l_strSQL & "VATNUM = '" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                        l_strSQL = l_strSQL & "SPEC1 = '000', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = '" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                        l_strSQL = l_strSQL & "SPEC3 = 'E', "
                    Case "7"
                        l_strSQL = l_strSQL & "VATNUM = '', "
                        l_strSQL = l_strSQL & "SPEC1 = '800', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = '', "
                        l_strSQL = l_strSQL & "SPEC3 = 'O', "
                    Case Else
                        l_strSQL = l_strSQL & "VATNUM = '', "
                        l_strSQL = l_strSQL & "SPEC1 = '100', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = 'GB', "
                        l_strSQL = l_strSQL & "SPEC3 = 'U', "
                End Select
                l_strSQL = l_strSQL & "SPEC6 = '" & StrReverse(Trim(" " & l_rstNewCustomers("BusinessUnit"))) & "', "
                l_strSQL = l_strSQL & "DETAILS = 'READY', "
                l_strSQL = l_strSQL & "EMAIL = '" & l_rstNewCustomers("email") & "', "
                l_strSQL = l_strSQL & "CUSTLOADED = '', "
                l_strSQL = l_strSQL & "SPECLOADED = '' "
                l_strSQL = l_strSQL & "WHERE ACCNAME = '" & ACCNUMBER & "';"

            End If
            
            Debug.Print l_strSQL
            
            'Post it to our local copy of the table for checking purposes
            cnn.Execute l_strSQL
            'Then post it to Israel and close that connection
            cnn2.Execute l_strSQL
            cnn2.Close
            
            Set rst = Nothing
            Set cnn2 = Nothing
            
        End If
        
        SetData "company", "postedtoaccountsdate", "companyID", l_rstNewCustomers("companyID"), Format(Now, "MM/DD/YYYY")
        l_rstNewCustomers.MoveNext
    Wend
    'Print a report of what we've done
    PrintCrystalReport "BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, False
    ExportCrystalReport "BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    
End If

l_rstNewCustomers.Close
Set l_rstNewCustomers = Nothing

PostNewCustomersToAccounts = True
App.LogEvent "Completed Posting New Customers"

PROC_EXIT:
Exit Function

PROC_ERROR_CUSTOMERS:
PostNewCustomersToAccounts = False
SendSMTPMail g_strAdministratorEmailAddress, "", "PostNewCustomersToAccounts generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Dim l_rstSequence As New ADODB.Recordset
    l_rstSequence.Open l_strSQL, cnn, 3, 3
    
    If Not l_rstSequence.EOF Then
        l_rstSequence.MoveFirst
        l_lngGetNextSequence = l_rstSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = '" & "CETA" & "', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        cnn.Execute l_strSQL
    End If
    
    l_rstSequence.Close
    Set l_rstSequence = Nothing
    
    GetNextSequence = l_lngGetNextSequence
        
End Function

Function JobSafeForPosting(lp_lngJobID As Long) As JobSafeForPostingLevel
    
    Dim l_strSQL As String
    
    'check the job for the client, and the client account code
    Dim l_strCompanyAccountCode As String
    If g_intWebInvoicing = 1 Then
        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("accountstransaction", "companyID", "accountstransactionID", lp_lngJobID))
    Else
        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
    End If
    
    If l_strCompanyAccountCode = "" Then
        JobSafeForPosting = vbuJobNotSafeCompanyAccountCode
        Exit Function
    End If
    
    'Check SAP code
    Dim l_strSAPcustomercode As String
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
    If l_strCompanyAccountCode = "" Then
        JobSafeForPosting = vbuJobNotSafeSAPcode
        Exit Function
    End If
    
    'Check PO number
    Dim l_strOrderNumber As String
    l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
    If Len(l_strOrderNumber) > 100 Then
        JobSafeForPosting = vbuJobNotSafePOTooLong
        Exit Function
    End If
    
    If g_intWebInvoicing = 1 Then
        'Check there is a PO number
        If GetData("accountstransaction", "clientponumber", "accountstransactionID", lp_lngJobID) = "" Then
            JobSafeForPosting = vbuJobNotSafeNoPONumber
            Exit Function
        End If
    End If
    
    'check the costing lines for valid account codes
    If g_intWebInvoicing = 1 Then
        l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE accountstransactionID = '" & lp_lngJobID & "' AND (accountcode IS NULL OR accountcode = '')"
    Else
        l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE jobID = '" & lp_lngJobID & "' AND (accountcode IS NULL OR accountcode = '')"
    End If
    Dim l_intInvalidCostingLines As Integer
    l_intInvalidCostingLines = GetCount(l_strSQL)
    
    If l_intInvalidCostingLines > 0 Then
        JobSafeForPosting = vbuJobNotSafeCostingRows
    Else
        JobSafeForPosting = vbuJobSafe
    End If
    
End Function

Function SendJobToAccounts(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long) As Boolean

'declare all required variables
Dim l_strCompanyAccountNumber As String, l_lngInvoiceNumber As Long, l_datInvoiceDate As Date, l_datInvoicePayDate As Date, l_strOrderNumber As String, l_strVATCode As String, l_strPriorityCode As String
Dim l_curTotal As Currency, l_curVAT As Currency, l_curTotalIncVAT As Currency, l_strItemAccountCode As String, l_strGeneralNotes As String, l_strCetaCode As String, l_strCetaStockCode As String, l_strCetaDescription As String
Dim l_strItemStockCode As String, l_strItemStockPriorityCode As String, l_curStockTotal As Currency, l_curStockVAT As Currency, l_curStockTotalIncVAT As Currency
Dim l_strCompanyName As String

Dim l_lngCompanyID As Long
Dim l_strSQL As String, l_strType As String

SendJobToAccounts = True

l_strType = lp_strType 'This gets changed during the function, and the calling routine needs for the parameter it sent to not change.
App.LogEvent "Posting Job Number: " & lp_lngJobID

'get the company ID as it is needed to work out other values
l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
l_strCompanyName = GetData("job", "companyname", "jobID", lp_lngJobID)

'get the company account number from company table - needs to be the salesledger code, not thew normal company code
l_strCompanyAccountNumber = GetData("company", "salesledgercode", "companyID", l_lngCompanyID)

'is this an invoice or a credit, as it makes a difference
Select Case lp_strType
Case "INVOICE"
    l_strType = "SI"
    l_lngInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "') ORDER BY costingID"
Case "CREDIT"
    l_strType = "SC"
    l_lngInvoiceNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "creditnotedate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE jobID = '" & lp_lngJobID & "'"
End Select

'add 30 days to invoice date for pay date
l_datInvoiceDate = Format(l_datInvoiceDate, "dd/mm/yyyy")
l_datInvoicePayDate = DateAdd("d", 30, l_datInvoiceDate)

'order number - if this is blank then send the customer contactname for the job

l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
If l_strOrderNumber = "" Then l_strOrderNumber = GetData("job", "contactname", "jobID", lp_lngJobID)
'vat code
l_strVATCode = GetData("company", "vatcode", "companyID", l_lngCompanyID)
If l_strVATCode = "" Then l_strVATCode = "1"

Select Case l_strVATCode
    Case "", "1"
        l_strVATCode = "T1"
    Case "G"
        l_strVATCode = "T4"
    Case "7"
        l_strVATCode = "T0"
End Select

'general notes
l_strGeneralNotes = GetData("job", "notes1", "jobID", lp_lngJobID)

'open up the costing table and loop through
Dim l_rstCosting As New ADODB.Recordset
l_rstCosting.Open l_strSQL, cnn, 3, 3

'if there are records, then move to the first one
If Not l_rstCosting.EOF Then l_rstCosting.MoveFirst

'start the loop
Do While Not l_rstCosting.EOF
    'pick up values for each line
    
    'standard fields
    If lp_strType = "INVOICE" Then
        l_curTotal = l_rstCosting("total")
        l_curVAT = l_rstCosting("vat")
        l_curTotalIncVAT = l_rstCosting("totalincludingvat")
        l_curStockTotal = Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    Else
        l_curTotal = 0 - l_rstCosting("total")
        l_curVAT = 0 - l_rstCosting("vat")
        l_curTotalIncVAT = 0 - l_rstCosting("totalincludingvat")
        l_curStockTotal = 0 - Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = 0 - Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = 0 - Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    End If
    
    l_strItemAccountCode = l_rstCosting("accountcode")
    l_strItemStockCode = Trim(" " & l_rstCosting("includedstocknominal"))
    l_strCetaCode = l_rstCosting("costcode")
    l_strCetaDescription = Trim(" " & l_rstCosting("description"))
    If l_strItemStockCode <> "" Then l_strCetaStockCode = GetData("ratecard", "cetacode", "nominalcode", l_strItemStockCode)
    l_strPriorityCode = Trim(" " & l_rstCosting("prioritycode"))
    l_strItemStockPriorityCode = Trim(" " & l_rstCosting("includedstockprioritycode"))
    If l_strItemStockPriorityCode <> "" Then l_strItemStockPriorityCode = GetData("ratecard", "cetacode", "prioritycode", l_strItemStockCode)
    
    'create a new record in the batch table
    If AddLineToBatch(lp_lngJobID, l_strType, l_strCompanyAccountNumber, l_strCompanyName, l_lngInvoiceNumber, l_datInvoiceDate, l_datInvoicePayDate, l_strOrderNumber, l_strVATCode, l_curTotal, l_curVAT, l_curTotalIncVAT, l_strItemAccountCode, l_strCetaCode, l_strCetaDescription, l_strGeneralNotes, lp_lngBatchNumber, l_strItemStockCode, l_strCetaStockCode, l_curStockTotal, l_curStockVAT, l_curStockTotalIncVAT, l_strPriorityCode, l_strItemStockPriorityCode) = False Then
        SendJobToAccounts = False
        Exit Function
    End If
    
    'move on to the next costing record
    l_rstCosting.MoveNext
Loop

'close the recordset
l_rstCosting.Close
Set l_rstCosting = Nothing

'is this a credit or an invoice? update the correct fields

'set the job and invoice header tables to include the batch number

Select Case lp_strType
Case "INVOICE"
    SetData "job", "senttoaccountsbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & "CETA" & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'INVOICE'"

Case "CREDIT"
    SetData "job", "creditnotebatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    SetData "job", "creditsenttoaccountsdate", "jobID", lp_lngJobID, FormatSQLDate(Now)
    SetData "job", "creditsenttoaccountsuser", "jobID", lp_lngJobID, "CETA"
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & "CETA" & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'CREDIT'"
End Select

'update the rows
cnn.Execute l_strSQL

End Function

Function AddLineToBatch(lp_lngJobID As Long, lp_strType As String, lp_strCompanyAccountCode As String, lp_strCompanyName As String, lp_lngInvoiceNumber As Long, _
                            lp_datInvoiceDate As Date, lp_datInvoicePayDate As Date, lp_strOrderNumber As String, lp_strVATCode As String, _
                            lp_curTotal As Currency, lp_curVAT As Currency, lp_curTotalIncVAT As Currency, lp_strAccountCode As String, lp_strCETACode As String, lp_strCetaDescription As String, _
                            lp_strGeneralNotes As String, lp_lngBatchNumber As Long, Optional lp_strStockCode As String, Optional lp_strCetaStockCode As String, _
                            Optional lp_curStockTotal As Currency, Optional lp_curStockVAT As Currency, Optional lp_curStockTotalIncVAT As Currency, Optional lp_strPriorityCode As String, Optional lp_strItemStockPriorityCode As String) As Boolean

If lp_curTotalIncVAT <> lp_curTotal + lp_curVAT Then
    AddLineToBatch = False
    Exit Function
End If

Dim l_rstBatchJob As New ADODB.Recordset
Dim l_strNewString As String
Dim l_strSQL As String
Dim l_strPriorityCode As String, l_strPriorityStockCode As String, l_strSAPcustomercode As String
Dim l_lngCompanyID As Long, l_strBusinessUnit As String
Dim l_strSageCustomerReference As String, l_lngSageCode As Long, l_lngSageStockCode As Long

On Error GoTo PROC_ERROR_LINE

If lp_curTotal <> 0 Then
    
    'Add an item to the batchsage table, for this item.
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_strSageCustomerReference = GetData("company", "SageCustomerReference", "companyID", l_lngCompanyID)
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", l_lngCompanyID)
    l_strBusinessUnit = GetData("company", "BusinessUnit", "companyID", l_lngCompanyID)
    l_lngSageCode = GetData("ProfitCentre5", "VDMS_Charge_Code", "ProfitCentre5ID", lp_strAccountCode)
    
    l_strSQL = "INSERT INTO batchsage (Type, AccountReference, NominalAccountReference, ItemDate, Reference, Details, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
    l_strSQL = l_strSQL & "'" & lp_strType & "', "
    l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
    l_strSQL = l_strSQL & l_lngSageCode & ", "
    l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
    l_strSQL = l_strSQL & lp_lngInvoiceNumber & ", "
    l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
    l_strSQL = l_strSQL & Format(lp_curTotal - lp_curStockTotal, "#.00") & ", "
    l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
    l_strSQL = l_strSQL & Format(lp_curVAT - lp_curStockVAT, "#.00") & ", "
    l_strSQL = l_strSQL & "1, "
    l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
    
    Debug.Print l_strSQL
    cnn.Execute l_strSQL
    
    If lp_curStockTotal <> 0 Then
        l_lngSageStockCode = GetData("ProfitCentre5", "VDMS_Charge_Code", "ProfitCentre5ID", lp_strStockCode)
        l_strSQL = "INSERT INTO batchsage (Type, AccountReference, NominalAccountReference, ItemDate, Reference, Details, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
        l_strSQL = l_strSQL & "'" & lp_strType & "', "
        l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
        l_strSQL = l_strSQL & l_lngSageStockCode & ", "
        l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & lp_lngInvoiceNumber & ", "
        l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
        l_strSQL = l_strSQL & Format(lp_curStockTotal, "#.00") & ", "
        l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
        l_strSQL = l_strSQL & Format(lp_curStockVAT, "#.00") & ", "
        l_strSQL = l_strSQL & "1, "
        l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
        Debug.Print l_strSQL
        cnn.Execute l_strSQL
    End If
    
    'open the batchjob table with no records and do the old style posting anyway
    l_strSQL = "SELECT * FROM batchjob WHERE batchjobID = -1"
    l_rstBatchJob.Open l_strSQL, cnn, 3, 3
        
    If lp_curStockTotal <> 0 Then

        'There is an included stock component of this line - split it into two lines for accounts
        'add a line to the batch table for original line
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SageCode") = l_lngSageCode
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal - lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT - lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT - lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

        'add a line to the batch table for the stock
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strStockCode
        l_rstBatchJob("SageCode") = l_lngSageStockCode
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    Else

        'add a line to the batch table
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        l_rstBatchJob("SageCode") = l_lngSageCode
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    End If
    
    'close the recordset
    l_rstBatchJob.Close
    Set l_rstBatchJob = Nothing
        
End If

AddLineToBatch = True

PROC_EXIT:

Exit Function

PROC_ERROR_LINE:

SendSMTPMail g_strAdministratorEmailAddress, "", "AddLineToBatch generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""
AddLineToBatch = False
Resume PROC_EXIT
        
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String

If IsNull(lp_varValue) Or lp_varValue = "" Or UCase(lp_varValue) = "NULL" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

cnn.Execute l_strSQL

End Function

Sub PrintCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_blnPreview As Boolean, Optional ByRef lp_lngCopies As Long, Optional ByRef lp_strPrinterToPrintTo As String)
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    'Check whether a number of copies was sent, and if not set to 1
    If lp_lngCopies = 0 Then
        lp_lngCopies = 1
    End If
    
    If lp_blnPreview = True Then lp_lngCopies = 1
    
    crxReport.DiscardSavedData
    
    If lp_strPrinterToPrintTo <> "" Then
        crxReport.PrinterSetup 0
    End If
        
    crxReport.PrintOut False, lp_lngCopies
        
    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Sub ExportCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_strExportFileName As String)
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    crxReport.DiscardSavedData
    
    crxReport.ExportOptions.DestinationType = crEDTDiskFile
    crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
    crxReport.ExportOptions.DiskFileName = lp_strExportFileName
    crxReport.ExportOptions.ExcelUseConstantColumnWidth = False
    crxReport.ExportOptions.ExcelUseFormatInDataOnly = True
    crxReport.ExportOptions.ExcelMaintainColumnAlignment = True
    crxReport.ExportOptions.ExcelMaintainRelativeObjectPosition = True
    crxReport.Export False

    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Public Function CurrentMachineName() As String
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
End Function

' Private function that does the work under Windows NT
Public Function GetUNCNameNT(pathName As String) As String

Dim hKey As Long
Dim hKey2 As Long
Dim exitFlag As Boolean
Dim i As Double
Dim errCode As Long
Dim rootKey As String
Dim Key As String
Dim computerName As String
Dim lComputerName As Long
Dim stPath As String
Dim firstLoop As Boolean
Dim Ret As Boolean

' first, verify whether the disk is connected to the network
If Mid(pathName, 2, 1) = ":" Then
   Dim UNCName As String
   Dim lenUNC As Long

   UNCName = String$(520, 0)
   lenUNC = 520
   errCode = WNetGetConnection(Left(pathName, 2), UNCName, lenUNC)

   If errCode = 0 Then
      UNCName = Trim(Left$(UNCName, InStr(UNCName, _
        vbNullChar) - 1))
      GetUNCNameNT = UNCName & Mid(pathName, 3)
      Exit Function
   End If
End If

' else, scan the registry looking for shared resources
'(NT version)
computerName = String$(255, 0)
lComputerName = Len(computerName)
errCode = GetComputerName(computerName, lComputerName)
If errCode <> 1 Then
   GetUNCNameNT = pathName
   Exit Function
End If

computerName = Trim(Left$(computerName, InStr(computerName, _
   vbNullChar) - 1))
rootKey = "SYSTEM\CurrentControlSet\Services\LanmanServer\Shares"
errCode = RegOpenKey(HKEY_LOCAL_MACHINE, rootKey, hKey)

If errCode <> 0 Then
   GetUNCNameNT = pathName
   Exit Function
End If

firstLoop = True

Do Until exitFlag
   Dim szValue As String
   Dim szValueName As String
   Dim cchValueName As Long
   Dim dwValueType As Long
   Dim dwValueSize As Long

   szValueName = String(1024, 0)
   cchValueName = Len(szValueName)
   szValue = String$(500, 0)
   dwValueSize = Len(szValue)

   ' loop on "i" to access all shared DLLs
   ' szValueName will receive the key that identifies an element
   errCode = RegEnumValue(hKey, i#, szValueName, _
       cchValueName, 0, dwValueType, szValue, dwValueSize)

   If errCode <> 0 Then
      If Not firstLoop Then
         exitFlag = True
      Else
         i = -1
         firstLoop = False
      End If
   Else
      stPath = GetPath(szValue)
      If firstLoop Then
         Ret = (UCase(stPath) = UCase(pathName))
         stPath = ""
      Else
         Ret = (UCase(stPath) = UCase(Left$(pathName, _
        Len(stPath))))
         stPath = Mid$(pathName, Len(stPath))
      End If
      If Ret Then
         exitFlag = True
         szValueName = Left$(szValueName, cchValueName)
         GetUNCNameNT = "\\" & computerName & "\" & _
            szValueName & stPath
      End If
   End If
   i = i + 1
Loop

RegCloseKey hKey
If GetUNCNameNT = "" Then GetUNCNameNT = pathName

End Function

' support routine
Private Function GetPath(st As String) As String
   Dim pos1 As Long, pos2 As Long, pos3 As Long
   Dim stPath As String

   pos1 = InStr(st, "Path")
   If pos1 > 0 Then
      pos2 = InStr(pos1, st, vbNullChar)
      stPath = Mid$(st, pos1, pos2 - pos1)
      pos3 = InStr(stPath, "=")
      If pos3 > 0 Then
         stPath = Mid$(stPath, pos3 + 1)
         GetPath = stPath
      End If
   End If
End Function

Function GetFlag(lp_intFlagValue As Variant)
    
    Select Case lp_intFlagValue
        Case 1, -1, True, "TRUE", "True", "true", "YES", "Yes", "yes"
            GetFlag = 1
        Case 0, "FALSE", "False", "false", "no", "NO", "No"
            GetFlag = 0
        Case Else
            GetFlag = 0
    End Select
    
End Function

Sub NotifyByEMail(lp_strBatchReport As String)

Dim rs As ADODB.Recordset, SQL As String
SQL = "SELECT value from Setting WHERE name = 'VDMSEmailForInvoices';"
Set rs = New ADODB.Recordset
rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    rs.MoveFirst
    Do While Not rs.EOF
        SendSMTPMail rs("value"), "VDMS Email For Invoices", "VDMS Acton Un-invoiced Work", "Uninvoices (all) Excel Sheet is attached", lp_strBatchReport, True, "", "", g_strAdministratorEmailAddress
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing

End Sub

Sub NotifyProblemByEmail(lp_lngJobID As Long, lp_strMessage As String)

Dim rs As ADODB.Recordset, SQL As String

SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'MX1' and trackermessageID = 28;"
Set rs = New ADODB.Recordset

rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    rs.MoveFirst
    While Not rs.EOF
        SendSMTPMail rs("email"), rs("fullname"), "MX1 Posting to Priority - Issue", lp_lngJobID & " will not be posted:" & vbCrLf & lp_strMessage, "", True, "", "", g_strAdministratorEmailAddress
        rs.MoveNext
    Wend
End If
rs.Close
Set rs = Nothing

End Sub

Sub ResetSentToAccountsOnJob(lp_lngJobID As Long, lp_strType As String)
    
Select Case lp_strType
    Case "INVOICE"
        SetData "job", "senttoaccountsdate", "jobID", lp_lngJobID, Null
        SetData "job", "senttoaccountsuser", "jobID", lp_lngJobID, Null
    Case "CREDIT"
        Dim l_lngCreditNoteNumber As Long
        l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
        SetData "invoiceheader", "senttoaccountsdate", "creditnotenumber", l_lngCreditNoteNumber, Null
    Case Else
        SetData "job", "senttoBBCdate", "jobID", lp_lngJobID, Null
End Select
        
End Sub

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function

Function CostJob(lp_lngJobID As Long, lp_blnCostDub As Boolean, lp_blnCostSchedule As Boolean, Optional lp_blnSilent As Boolean)
        
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_blnJobDetailOnInvoice As Boolean
    Dim l_blnCostingsExist As Boolean
    Dim l_strSQL As String
    Dim l_lngCompanyID As Long
    Dim l_intRatecard As Integer
    
    Dim l_strDescription As String
    Dim l_strFormat As String
    Dim l_dblQuantity As Double
    Dim l_dblUnitCharge As Double
    Dim l_sngDiscount As Single
    Dim l_sngMachineTime As Single
    Dim l_dblMinimumCharge As Double
    Dim l_dblRateCardMinimumCharge As Double
    
    Dim l_lngJobDetailID As Long
    Dim l_strCopyType As String
    Dim l_sngVATRate As Single
    
    Dim l_lngQuoteID As Long
    
    Dim l_intTwoTierPricing As Integer
    
    Dim l_lngMasterResourceScheduleID  As Long
    Dim l_intShowOnQuote  As Integer
    

    Dim l_lngResourceScheduleID As Long
    Dim l_rstResourceSchedule As ADODB.Recordset
    
    Dim l_dblRateCardPrice As Double
    Dim l_lngResourceID As Long
    
    Dim l_intLoop As Integer
    Dim l_lngCount As Long
    Dim l_rsChargeBands As ADODB.Recordset, l_strChargeBandName As String
    
    'check to see if product costing should be used
    
    Dim l_lngProductID As Long
    l_lngProductID = GetData("job", "productID", "jobID", lp_lngJobID)
    
    'if not, just keep the product ID as 0 and it will ignore the code
    If g_optUseProductRateCards = 0 Then l_lngProductID = 0
    
    'mousepointer
    Screen.MousePointer = vbHourglass
    
    'get the settings
    l_blnJobDetailOnInvoice = GetFlag(GetData("job", "flagdetailoninvoice", "jobID", lp_lngJobID))
    
    Dim l_strJobType  As String
    l_strJobType = GetData("job", "jobtype", "jobID", lp_lngJobID)
    
    'check if there are already costings
    l_blnCostingsExist = CostingsExist(lp_lngJobID)
    
    'pick up the company ID
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    
    'find out which rate card they use
    l_intRatecard = Val(GetData("company", "ratecard", "companyID", l_lngCompanyID))

    'make sure we use at least the bas rate card
    If l_intRatecard > 9 Or l_intRatecard < 1 Then
        l_intRatecard = g_intBaseRateCard
    End If
    
    Dim l_intDoNotChargeVAT As Integer
    l_intDoNotChargeVAT = GetData("job", "donotchargevat", "jobID", lp_lngJobID)
    
'    'are they on two - tier pricing
'    l_intTwoTierPricing = InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/two-tier-pricing")
'
'    If l_intTwoTierPricing <> 0 Then
'        l_intTwoTierPricing = True
'    End If

    'get their vat rate
    l_sngVATRate = GetVATRate(Format(GetData("company", "vatcode", "companyID", l_lngCompanyID)))
    
    'check for the actual tick box on the form
    If l_intDoNotChargeVAT <> 0 Then
        l_sngVATRate = 0
    End If
    
    
    'is there a quote ID to use for this job
    l_lngQuoteID = GetData("project", "quoteID", "projectID", GetJobProjectID(lp_lngJobID))
    
    'if there are NO costings...
    If l_blnCostingsExist = False Then
        
        'if we want to cost schedule items
        If lp_blnCostSchedule <> False Or UCase(l_strJobType) = "HIRE" Or UCase(l_strJobType) = "EDIT" Then
            
            'should we skip the costing of resource schedules
            If UCase(Left(l_strJobType, 3)) = "DUB" And g_intDontCostScheduledResourcesIfJobTypeIsADub = 1 Then GoTo PROC_SkipSchedule
            
            l_strSQL = "SELECT masterresourcescheduleID, showonquote, resourcescheduleID, resourceID, actualhoursused, starttime, endtime, costcode, hiredescription FROM resourceschedule WHERE jobID = '" & lp_lngJobID & "' ORDER BY resourcescheduleID"
            Set l_rstResourceSchedule = New ADODB.Recordset
            l_rstResourceSchedule.Open l_strSQL, cnn, 3, 3
            
            'cycle through and cost each item
            If Not l_rstResourceSchedule.EOF Then l_rstResourceSchedule.MoveFirst
            
            'loop through all the resource schedule items
            Do While Not l_rstResourceSchedule.EOF
            
                'pick up the cost code for this resource
                If Trim(" " & l_rstResourceSchedule("hiredescription")) <> "" Then
                    l_strFormat = l_rstResourceSchedule("costcode")
                Else
                    l_strFormat = GetData("resource", "nominalcode", "resourceID", l_rstResourceSchedule("resourceID"))
                End If
                
                'if there is a valid cost code and its not NO COST
                If (l_strFormat <> "") And (UCase(l_strFormat) <> "NO COST") Then
                    
                    'set the quantity to = 1
                    l_dblQuantity = 1
                    
                    'get the resource schedule ID
                    l_lngResourceScheduleID = l_rstResourceSchedule("resourcescheduleID")
                    
                    'type R for resource schedule
                    l_strCopyType = "R"
                    
                    'get the master resource schedule ID
                    l_lngMasterResourceScheduleID = Val(Format(l_rstResourceSchedule("masterresourcescheduleID")))
                    
                    'check if this item should be costed (kits mainly)
                    l_intShowOnQuote = Val(Format(l_rstResourceSchedule("showonquote")))
                    
                    'need to check for the type of unit that they are charging for:
                    l_sngMachineTime = Val(Format(l_rstResourceSchedule("actualhoursused"))) '* 60
                                        
                    Dim l_strChargeUnit As String
                    l_strChargeUnit = GetData("job", "chargeunits", "jobID", lp_lngJobID)
                    
                    
                    'use the booked duration to calculate the costs
                    If l_sngMachineTime = 0 And (UCase(l_strJobType) = "HIRE" Or UCase(l_strJobType) = "EDIT") Then
                        l_strChargeUnit = "day"
                        l_sngMachineTime = DateDiff("h", l_rstResourceSchedule("starttime"), l_rstResourceSchedule("endtime"))
                    End If
                    
                    'change the machine time to the correct number
                    Select Case UCase(l_strChargeUnit)
                    Case "DAY"
                        l_sngMachineTime = l_sngMachineTime / 24
                    Case "WEEK"
                        l_sngMachineTime = l_sngMachineTime / 24 / 7
                    Case "HOUR"
                        'stays the same as hours are assumed
                    Case "1/2 DAY"
                        l_sngMachineTime = l_sngMachineTime * 2
                    Case Else
                        l_strChargeUnit = "HOUR" 'just use the default
                    End Select
                                    
                    If UCase(g_strRateCardType) <> "HIRE" Then
                                    
                        Dim l_strUnit As String
                        l_strUnit = UCase(GetDataSQL("SELECT TOP 1 unit FROM ratecard WHERE cetacode = '" & l_strFormat & "' AND companyID = '0'"))
                            
                        If Left(l_strUnit, 4) = "WEEK" Then
                            If UCase(l_strChargeUnit) = "DAY" Then
                                If l_sngMachineTime < 7 Then
                                    l_sngMachineTime = 1
                                Else
                                    l_sngMachineTime = l_sngMachineTime / 7
                                End If
                            End If
                        End If
                        
                        If Left(l_strUnit, 3) = "DAY" Then
                            If UCase(l_strChargeUnit) = "WEEK" Then
                                l_sngMachineTime = l_sngMachineTime * 4
                            End If
                        End If
                    Else
                        l_strUnit = l_strChargeUnit
                    End If
                    
                    'update the machine time to be 1 if its currently 0
                    If l_sngMachineTime = 0 Then l_sngMachineTime = 1
                    
                    'set the base charge to be 0
                    l_dblUnitCharge = 0
                    
                    'get the resource ID
                    l_lngResourceID = Format(l_rstResourceSchedule("resourceID"))
                    
                    'if you are using a quote, then it should get the quoted rates, rather than the rate card rates
                                        
                    'if there was no quote, get the ratecard price
                    
                    l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                                    
                    l_dblUnitCharge = GetPrice(l_strFormat, CStr(l_intRatecard), l_lngCompanyID, l_strChargeUnit, l_lngProductID)
                    
                    If l_dblUnitCharge = 0 Then
                        l_dblUnitCharge = GetPrice(l_strFormat, CStr(l_intRatecard), l_lngCompanyID, l_lngProductID)
                    End If
                
                    'get the basic ratecard price
                    If UCase(g_strRateCardType) = "HIRE" Then
                        l_dblRateCardPrice = GetPrice(l_strFormat, g_intBaseRateCard, 0, l_strChargeUnit, 0)
                    Else
                        l_dblRateCardPrice = GetPrice(l_strFormat, g_intBaseRateCard, 0, "", 0)
                    End If
                    
                    'Get the Minimum charge if it exists
                    
                    l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                    
                    'use the ratecard description
                    If g_optAlwaysUseRateCardDescriptions = 1 Then
                        l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                    Else
                        l_strDescription = Format(GetData("resource", "description", "resourceID", l_lngResourceID))
                        If l_strDescription = "" Then
                            l_strDescription = Format(GetData("resource", "name", "resourceID", l_lngResourceID))
                        End If
                    End If
                    
                    If g_optDeductPreSetDiscountsFromUnitPrice Then
                        'hide the discount, and make it look like the standard rate
                        l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                        l_sngDiscount = 0
                    End If

                    'if this is  a kit item, set the cost to be ZERO
                    
                    If l_lngMasterResourceScheduleID <> 0 Then
                        l_dblRateCardPrice = 0
                        l_dblUnitCharge = 0
                    End If
                    
                    If l_intShowOnQuote <> 0 Or l_lngMasterResourceScheduleID = 0 Then
                        'add the stock to the costing
                        CreateCostingLine l_lngResourceScheduleID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge, , l_strUnit
                    End If
                End If
                
                'cycle
                l_rstResourceSchedule.MoveNext
            Loop
            
            l_rstResourceSchedule.Close
            Set l_rstResourceSchedule = Nothing
            

        End If
        
PROC_SkipSchedule:
        
        'should we do the dub style costings?
        
        If lp_blnCostDub = True Then
            
            'do the query on the jobdetail table
            
            l_strSQL = "SELECT * FROM jobdetail WHERE jobID = '" & lp_lngJobID & "' AND copytype <> 'T' ORDER BY fd_orderby, jobdetailID"
            'open the recordset
            Dim l_rstJobDetail As New ADODB.Recordset
            l_rstJobDetail.Open l_strSQL, cnn, 3, 3
            
            If Not l_rstJobDetail.EOF Then
                l_rstJobDetail.MoveFirst
            End If
            
            Do While Not l_rstJobDetail.EOF
                
'                If (g_intWebInvoicing = 1 And GetData("costing", "costingID", "jobdetailID", l_rstJobDetail("jobdetailID")) = 0) Or g_intWebInvoicing = 0 Then
                    l_lngJobDetailID = l_rstJobDetail("jobdetailID")
                    l_strFormat = Format(l_rstJobDetail("format"))
                    
                    If Not IsNull(l_rstJobDetail("runningtime")) Then
                        If InStr(l_rstJobDetail("runningtime"), ":") <> 0 Then
                            l_sngMachineTime = Left(l_rstJobDetail("runningtime"), InStr(l_rstJobDetail("runningtime"), ":") - 1)
                        Else
                            l_sngMachineTime = Val(l_rstJobDetail("runningtime"))
                        End If
                        
                    Else
                        l_sngMachineTime = 0
                    End If
                    
                    l_strCopyType = UCase(Format(l_rstJobDetail("copytype")))
                    
                    'need to check here to see if the job is going to be charged per minute, or per 1/4 hour, and check whether valid duration entered
                    
                    If l_strCopyType = "M" Or l_strCopyType = "C" Or l_strCopyType = "A" Or l_strCopyType = "I" Then
                        If l_sngMachineTime = 0 Then
                            If g_optAbortIfAZeroDuration <> 0 Then
                                If lp_blnSilent = False Then MsgBox "There are lines with Zero duration that must be corrected!", vbCritical, "Costing Aborted"
                                GoTo Proc_Check_Record_Error
                            End If
                        End If
                    End If
                    
                    If l_strCopyType = "I" Or l_strCopyType = "IS" Then
                        l_strChargeBandName = GetData("xref", "information", "format", l_strFormat)
                        Set l_rsChargeBands = New ADODB.Recordset
                        l_rsChargeBands.Open "SELECT Top 1 * FROM CostingChargeBandEntry WHERE Bandname = '" & l_strChargeBandName & "' AND LowestTime <= " & l_sngMachineTime & " AND HighestTime >= " & l_sngMachineTime, cnn, 3, 3
                        If l_rsChargeBands.RecordCount <= 0 Then
                            l_sngMachineTime = Int((l_sngMachineTime + 12) / 15) * 15
                            If l_sngMachineTime < 15 Then l_sngMachineTime = 15
                        Else
                            l_sngMachineTime = l_rsChargeBands("ChargeBracket")
                        End If
                        l_rsChargeBands.Close
                        Set l_rsChargeBands = Nothing
                    ElseIf LCase(GetData("job", "chargeunits", "jobID", lp_lngJobID)) <> "mins" Then
                        If GetDataSQL("SELECT TOP 1 format FROM xref WHERE description = '" & l_strFormat & "' AND category = 'dformat';") = "30" Then
                            l_sngMachineTime = Int((l_sngMachineTime + 27) / 30) * 30
                            If l_sngMachineTime < 30 Then l_sngMachineTime = 30
                        ElseIf IsNull(l_rstJobDetail("chargerealminutes")) Or l_rstJobDetail("chargerealminutes") = False Then
                            l_sngMachineTime = Int((l_sngMachineTime + 12) / 15) * 15
                            If l_sngMachineTime < 15 Then l_sngMachineTime = 15
                        End If
                    End If
                    
                    
                    'do we want to work out the duration / cost too?
                    If g_optAddDurationToRateCode <> 0 Or l_strCopyType = "I" Or l_strCopyType = "IS" Then
                        l_strFormat = l_strFormat & Format(l_sngMachineTime, "000")
                        l_sngMachineTime = 1
                    Else
                        If g_optDontMakeMachineTimeADecimal = 0 Then l_sngMachineTime = l_sngMachineTime / 60
                    End If
                    
                    l_dblQuantity = Val(Format(l_rstJobDetail("quantity")))
                    l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                    l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strFormat, l_lngCompanyID)
                    
                    l_dblRateCardPrice = GetUnitCharge(g_intBaseRateCard, l_strFormat, 0)
                    
                    l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                    l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                    
                    'either use the ratecard description or use the standard ie. "Digi playback 20 mins"
                    If g_optUseAdditionalDescriptionsWhenCosting Then
                        l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                    Else
                        If g_optDontMakeMachineTimeADecimal = 1 Then
                            l_strDescription = RTrim(l_rstJobDetail("format")) & " playback " & l_sngMachineTime & " mins"
                        Else
                            l_strDescription = RTrim(l_rstJobDetail("format")) & " playback " & l_sngMachineTime * 60 & " mins"
                        End If
                    End If
                    
                    Select Case l_strCopyType
                        Case "M" 'master
                            'use the job detail description on the invoice for the master
                            If g_optUseMasterDetailOnInvoice Then
                                l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            End If
                            
                        Case "C"
                            'if they want descriptions on all lines (set on job)
                            If l_blnJobDetailOnInvoice <> 0 Then
                                l_strDescription = l_rstJobDetail("description")
                            Else
                                If g_optDontMakeMachineTimeADecimal = 1 Then
                                    l_strDescription = RTrim(l_rstJobDetail("format")) & " record " & l_sngMachineTime & " mins"
                                Else
                                    l_strDescription = RTrim(l_rstJobDetail("format")) & " record" & l_sngMachineTime * 60 & " mins"
                                End If
                            End If
                            
                        Case "O", "I", "IS"
                            
                            'l_strDescription = Trim(l_rstJobDetail("description") & " - " & l_rstJobDetail("workflowdescription"))
                            l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            l_sngMachineTime = 1
                                                        
                        Case "A"
                            If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/progress") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/tracker") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/dadctracker") > 0 Or InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/DADCFixes") > 0 Then
                                l_strDescription = Trim(" " & l_rstJobDetail("description"))
                            End If
                            
                        Case "T"
                            
                    End Select
                    
                    If g_optDeductPreSetDiscountsFromUnitPrice Then
                        'hide the discount, and make it look like the standard rate
                        l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                        l_sngDiscount = 0
                    End If
                    
                    CreateCostingLine l_lngJobDetailID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge
    
                    'Cost the stock for this line
                    If UCase(l_rstJobDetail("copytype")) = "C" Then
                        
                        'get both stock 1 and stock 2
                        For l_intLoop = 1 To 2
                            l_dblQuantity = Val(Format(l_rstJobDetail("stock" & l_intLoop & "ours")))
                            
                            'if there is a quantity of stock
                            If l_dblQuantity > 0 Then
                                
                                'set variables
                                If Trim(" " & l_rstJobDetail("stock" & l_intLoop & "type")) = "" Then
                                    MsgBox "There is an error with one of the stock codes - any line with a quantity of stock must have a stock code", vbCritical, "Error COsting Job"
                                    Exit Function
                                End If
                                l_lngJobDetailID = l_rstJobDetail("jobdetailID")
                                l_strCopyType = "S"
                                l_strDescription = ""
                                l_strFormat = l_rstJobDetail("stock" & l_intLoop & "type")
                                l_sngMachineTime = 0
                                l_sngDiscount = GetCompanyDiscount(l_lngCompanyID, l_strFormat)
                                l_dblUnitCharge = GetUnitCharge(l_intRatecard, l_strFormat, l_lngCompanyID)
                                l_dblRateCardPrice = GetUnitCharge(g_intBaseRateCard, l_strFormat, l_lngCompanyID)
                                l_dblMinimumCharge = GetMinimumCharge(l_strFormat, l_lngCompanyID)
                                l_dblRateCardMinimumCharge = GetMinimumCharge(l_strFormat, 0)
                                
                                'use the ratecard description
                                l_strDescription = Format(GetData("ratecard", "description", "cetacode", l_strFormat))
                                
                                If g_optDeductPreSetDiscountsFromUnitPrice Then
                                    'hide the discount, and make it look like the standard rate
                                    l_dblUnitCharge = (l_dblUnitCharge / 100) * (100 - l_sngDiscount)
                                    l_sngDiscount = 0
                                End If
                                
                                'add the stock to the costing
                                CreateCostingLine l_lngJobDetailID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblRateCardPrice, l_dblMinimumCharge, l_dblRateCardMinimumCharge
                            End If
                        Next
                    End If
                    
'                End If

                'cycle through the records
                l_rstJobDetail.MoveNext
                
                DoEvents
                
            Loop
                        
Proc_Check_Record_Error:
            
            l_rstJobDetail.Close
            Set l_rstJobDetail = Nothing
            
        End If ' end of dub costing
        
        If g_optCostDespatch <> False Then
            
            l_strSQL = "SELECT despatchID, postcode, charge, companyname, despatchnumber, purchaseheaderID, direction, costcode  FROM despatch WHERE jobID = '" & lp_lngJobID & "' ORDER BY despatchID"
            
            Dim l_strPostCode As String
            Dim l_dblCharge As Double
            Dim l_lngDespatchID As Long
            Dim l_strCompany  As String
            Dim l_strPONumber As Long
            
            Dim l_rstDespatch As ADODB.Recordset
            Set l_rstDespatch = New ADODB.Recordset
            l_rstDespatch.Open l_strSQL, cnn, 3, 3
            
            'cycle through and cost each item, if required
            'go back and cost all the stock on the job
            If Not l_rstDespatch.EOF Then l_rstDespatch.MoveFirst
           
            Do While Not l_rstDespatch.EOF
                
                    l_strPONumber = Format(l_rstDespatch("purchaseheaderID"))
                    Dim l_lngDespatchNumber  As Long
                    l_strCompany = l_rstDespatch("companyname")
                    l_lngDespatchID = l_rstDespatch("despatchID")
                    l_lngDespatchNumber = l_rstDespatch("despatchnumber")
                    l_strPostCode = l_rstDespatch("postcode")
                    l_dblCharge = Val(Format(l_rstDespatch("charge")))
                    l_strFormat = Format(l_rstDespatch("costcode"))
                    
                    l_strCopyType = "X"
                    
                    Dim l_strPurchaseOrder As String
                    If l_strPONumber <> 0 Then
                        l_strPurchaseOrder = " PO# " & l_strPONumber
                    Else
                        l_strPurchaseOrder = " "
                    End If
                    
                    If l_rstDespatch("direction") = "OUT" Then
                        l_strDescription = "Delivery to " & l_strCompany & " @ " & l_strPostCode & l_strPurchaseOrder & " (DNote: " & l_lngDespatchNumber & ")"
                    Else
                        l_strDescription = "Collection from " & l_strCompany & " @ " & l_strPostCode & l_strPurchaseOrder & "(DNote: " & l_lngDespatchNumber & ")"
                    End If
                    
                    If l_strFormat = "" Then l_strFormat = "DELIVERY"
                    
                    l_dblQuantity = 1
                    l_sngMachineTime = 1
                    l_dblUnitCharge = l_dblCharge
                    l_sngDiscount = 0
                    
                    
                    'add the stock to the costing
                    CreateCostingLine l_lngDespatchID, lp_lngJobID, l_strCopyType, l_strDescription, l_strFormat, l_dblQuantity, l_sngMachineTime, l_dblUnitCharge, l_sngDiscount, l_sngVATRate, l_intTwoTierPricing, l_dblCharge, 0, 0
                
                'cycle
                l_rstDespatch.MoveNext
            Loop
            
            l_rstDespatch.Close
            Set l_rstDespatch = Nothing
        End If

        
        'ShowJob lp_lngJobID, frmJob.tab1.Tab
    
    
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description, vbExclamation, CStr(Err.Number)
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function CostingsExist(lp_lngJobID As Long) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT count(costingID) FROM costing WHERE jobID = " & lp_lngJobID
    
'    If g_intWebInvoicing = 1 Then
'        l_strSQL = l_strSQL & " AND accountstransactionID = 0"
'    End If
    
    Dim l_rstCosting As New ADODB.Recordset
    l_rstCosting.Open l_strSQL, cnn, 3, 3
    
    If l_rstCosting(0) = 0 Then
        CostingsExist = False
    Else
        CostingsExist = True
    End If
    
    l_rstCosting.Close
    Set l_rstCosting = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetVATRate(lp_strVATCode As String) As Single
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT vatrate FROM vatrate WHERE vatcode = '" & lp_strVATCode & "'"
    
    Dim l_rstGetData As New ADODB.Recordset
    l_rstGetData.Open l_strSQL, cnn, 3, 3
    
    If Not l_rstGetData.EOF Then
        GetVATRate = Format(l_rstGetData("vatrate"))
    Else
        'Vat code didn't exist - tell them and then set it to code 1
        'MsgBox "VAT Code '" & lp_strVATCode & "' does not exist - setting Vat rate to Base UK VAT", vbInformation, "VAT Rate"
        l_strSQL = "SELECT vatrate FROM vatrate WHERE vatcode = '1'"
        l_rstGetData.Close
        l_rstGetData.Open l_strSQL, cnn, 3, 3
        If Not l_rstGetData.EOF Then
            GetVATRate = Format(l_rstGetData("vatrate"))
        Else
            MsgBox "Could not load base UK VAT Rate.", vbCritical, "VAT Rate"
            GetVATRate = 0
        End If
    End If
    
    l_rstGetData.Close
    Set l_rstGetData = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetJobProjectID(lp_lngJobID As Long)

Dim l_lngProjectID As Long

l_lngProjectID = GetData("job", "projectID", "jobID", lp_lngJobID)

GetJobProjectID = l_lngProjectID

End Function

Function GetCompanyDiscount(ByVal lp_lngCompanyID As Long, ByVal lp_strCETACode As String) As Single
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    l_strSQL = "SELECT discount FROM discount WHERE companyID = " & lp_lngCompanyID & " AND cetacode = '" & lp_strCETACode & "'"
    
    Dim l_rstDiscount As New ADODB.Recordset
    l_rstDiscount.Open l_strSQL, cnn, 3, 3
    
    If Not l_rstDiscount.EOF Then
        GetCompanyDiscount = l_rstDiscount("discount")
    Else
        'If no specific discount fourn then check if there is a general discount for this company in the company table.
        l_rstDiscount.Close
            
        l_strSQL = "SELECT masterdiscount FROM company WHERE companyID = " & lp_lngCompanyID
        
        l_rstDiscount.Open l_strSQL, cnn, 3, 3
        
        If Not l_rstDiscount.EOF Then
            GetCompanyDiscount = Val(Trim(" " & l_rstDiscount("masterdiscount")))
        Else
            GetCompanyDiscount = 0
        End If
    End If
    
    l_rstDiscount.Close
    Set l_rstDiscount = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetPrice(ByVal lp_strCostCode As String, ByVal lp_strRateCardColumn As String, ByVal lp_lngCompanyID As Long, Optional ByVal lp_strUnit As String, Optional ByVal lp_lngProductID As Long)

Dim l_dblPrice As Double
Dim l_strAdditionalSQL  As String

If UCase(g_strRateCardType) = "HIRE" Then GoTo PROC_HireRateCard

PROC_StandardRateCard:

If lp_lngProductID = 0 Then

    'check to see if a unit was specified
    If lp_strUnit <> "" Then
        l_strAdditionalSQL = " AND unit = '" & QuoteSanitise(lp_strUnit) & "'"
    End If

PROC_StandardCosting:
    'normal costing - not using the new product rate cards
    
    l_dblPrice = GetDataSQL("SELECT TOP 1 price" & lp_strRateCardColumn & " FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";")
    
    If l_dblPrice = 0 Then
        l_dblPrice = GetDataSQL("SELECT TOP 1 price" & lp_strRateCardColumn & " FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";")
    End If
    
    GoTo PROC_COMPLETE

Else
    'use theproduct based rate cards when a product ID is specified

PROC_ProductCosting:

    Dim l_lngCustomRateCardID As Long
    l_lngCustomRateCardID = GetData("product", "customratecardID", "productID", lp_lngProductID)
    
    'if there is no custom rate card them skip to standard costing
    If l_lngCustomRateCardID = 0 Then GoTo PROC_StandardCosting
    
    Dim l_lngCustomRateCardDetailID  As Long
    
    'get the price for this unit type
    l_lngCustomRateCardDetailID = Val(GetDataSQL("SELECT TOP 1 customratecarddetailID FROM customratecarddetail WHERE customratecardID = '" & l_lngCustomRateCardID & "' AND costcode = '" & QuoteSanitise(lp_strCostCode) & "'" & l_strAdditionalSQL & ";"))
    
    'found a costing record so use this price
    If l_lngCustomRateCardDetailID > 0 Then
        l_dblPrice = GetData("customratecarddetail", "price", "customratecarddetailID", l_lngCustomRateCardDetailID)
        GoTo PROC_COMPLETE
    End If
    
    'still no price, try without the unit type.
    l_lngCustomRateCardDetailID = Val(GetDataSQL("SELECT TOP 1 customratecarddetailID FROM customratecarddetail WHERE customratecardID = '" & l_lngCustomRateCardID & "' AND costcode = '" & QuoteSanitise(lp_strCostCode) & "';"))
    
    If l_lngCustomRateCardDetailID > 0 Then
        l_dblPrice = GetData("customratecarddetail", "price", "customratecarddetailID", l_lngCustomRateCardDetailID)
        GoTo PROC_COMPLETE
    Else
        'no price found still so do the normal costing
        GoTo PROC_StandardCosting
    End If
    
End If

PROC_HireRateCard:

If lp_strUnit = "" Then GoTo PROC_StandardRateCard

    Select Case LCase(lp_strUnit)
    Case "each", "hour", "halfday", "day", "week", "month"
    
        l_dblPrice = GetDataSQL("SELECT TOP 1 " & lp_strUnit & "rate FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
        
        If l_dblPrice = 0 Then
            l_dblPrice = GetDataSQL("SELECT TOP 1 " & lp_strUnit & "rate FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
        End If

    Case Else
        GoTo PROC_StandardRateCard
        
    End Select

PROC_COMPLETE:

GetPrice = l_dblPrice

End Function

Function GetMinimumCharge(lp_strCostCode As String, lp_lngCompanyID As Long) As Double

Dim l_dblPrice As Double, l_rst As ADODB.Recordset

Set l_rst = New ADODB.Recordset
l_rst.Open "SELECT minimumcharge FROM ratecard WHERE companyID = '" & lp_lngCompanyID & "' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';", cnn, 3, 3

If l_rst.RecordCount > 0 Then
    l_dblPrice = l_rst(0)
Else
    l_dblPrice = GetDataSQL("SELECT TOP 1 minimumcharge FROM ratecard WHERE companyID = '0' AND cetacode = '" & QuoteSanitise(lp_strCostCode) & "';")
End If

l_rst.Close
Set l_rst = Nothing

GetMinimumCharge = l_dblPrice

End Function

Function CreateCostingLine(ByVal lp_lngItemID As Long, ByVal lp_lngJobID As Long, ByVal lp_strCopyType As String, ByVal lp_strDescription As String, _
                        ByVal lp_strCETACostCode As String, ByVal lp_dblQuantity As Double, ByVal lp_sngTime As Single, ByVal lp_dblUnitCharge As Double, _
                        ByVal lp_sngDiscount As Single, ByVal lp_sngVATRate As Single, ByVal lp_blnTwoTier As Integer, ByVal lp_dblRateCardPrice As Double, _
                        ByVal lp_dblMinimumCharge As Double, ByVal lp_dblRatecardMinimumCharge As Double, Optional ByVal lp_lngCostingDetailID As Long, _
                        Optional ByVal lp_strUnit As String) As Long

'TVCodeTools ErrorEnablerStart
On Error GoTo PROC_ERR
'TVCodeTools ErrorEnablerEnd
    
    Dim l_strDescription As String
    Dim l_strUnit As String
    
    l_strDescription = QuoteSanitise(lp_strDescription)
    
    If lp_strCopyType <> "S" Then
        
        'delete any existing line for this jobdetailID
        Dim l_strSQL As String
        l_strSQL = "DELETE FROM costing WHERE jobdetailID = " & lp_lngItemID & " AND copytype = '" & lp_strCopyType & "'"
        
        cnn.Execute l_strSQL
        
    End If
    
    Dim l_dblRateCardTotal As Double
    
    Dim l_strAccountCode As String, l_strStockNominalCode As String, l_dblStockUnitCharge  As Double
    Dim l_strPriorityCode As String, l_strPriorityStockCode As String
    l_strAccountCode = Trim(" " & GetData("ratecard", "nominalcode", "cetacode", lp_strCETACostCode))
    l_strStockNominalCode = Trim(" " & GetData("ratecard", "includedstocknominalcode", "cetacode", lp_strCETACostCode))
    If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/posttononrevenuebalancesheet") > 0 Then
        l_strPriorityCode = "1464002"
    Else
        l_strPriorityCode = GetDataSQL("SELECT TOP 1 prioritycode from ratecard where cetacode = '" & lp_strCETACostCode & "' AND companyID = " & GetData("job", "companyID", "jobID", lp_lngJobID))
        If l_strPriorityCode = "" Then l_strPriorityCode = GetData("ratecard", "prioritycode", "cetacode", lp_strCETACostCode)
    End If
    l_strPriorityStockCode = GetData("ratecard", "prioritycode", "nominalcode", l_strStockNominalCode)
    If l_strPriorityStockCode <> "" Then
        If InStr(GetData("company", "cetaclientcode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID)), "/posttononrevenuebalancesheet") > 0 Then
            l_strPriorityStockCode = "1464002"
        End If
    End If
    l_dblStockUnitCharge = Val(Trim(" " & GetData("ratecard", "includedstockprice", "cetacode", lp_strCETACostCode)))
    
    'in here sort out quantity thing... if need be...
    If g_optHideTimeColumn = 1 Then
        lp_dblQuantity = lp_sngTime
        lp_sngTime = 1
    End If
    
    If lp_strCopyType = "O" Or lp_strCopyType = "T" Or lp_strCopyType = "P" Then
    
        If lp_strCopyType = "R" Then lp_dblQuantity = (lp_dblQuantity * lp_sngTime)
        
        If g_optDontMakeMachineTimeADecimal = 1 Then
            lp_sngTime = 60
        Else
            lp_sngTime = 1
        End If
        
    End If
    
    If lp_strCopyType = "I" Or lp_strCopyType = "IS" Then
        lp_sngTime = 0
    End If
    
    If lp_strUnit <> "" Then
        l_strUnit = lp_strUnit
    Else
        l_strUnit = GetData("ratecard", "unit", "cetacode", lp_strCETACostCode)
    End If
    
    'work out the base ratecard total
    If lp_strCopyType <> "I" And lp_strCopyType <> "IS" Then
        If g_optDontMakeMachineTimeADecimal = 1 Then
            If lp_strCopyType <> "S" Then
                If l_strUnit <> "M" Then
                    l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * (lp_sngTime / 60)
                Else
                    l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * lp_sngTime
                End If
            Else
                l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity
            End If
        Else
            l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity * lp_sngTime
        End If
    Else
        l_dblRateCardTotal = lp_dblRateCardPrice * lp_dblQuantity
    End If
    
    If l_dblRateCardTotal < lp_dblRatecardMinimumCharge Then l_dblRateCardTotal = lp_dblRatecardMinimumCharge
    
    If l_strDescription = "" Or g_optAlwaysUseRateCardDescriptions <> 0 Then
        If lp_strCopyType <> "X" And UCase(Left(lp_strCETACostCode, 3)) <> "DEL" Then l_strDescription = GetData("ratecard", "description", "cetacode", lp_strCETACostCode)
    End If
    
    
    'work out the actual total
    Dim l_dblTotal As Double
    Dim l_sngTime2 As Single
    
    If lp_sngTime = 0 Then
        l_dblTotal = lp_dblUnitCharge * lp_dblQuantity
        l_sngTime2 = g_intDefaultTimeWhenZero
    Else
        l_sngTime2 = lp_sngTime
        If l_strUnit = "M" Then
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime))
        ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime / 60))
        Else
            l_dblTotal = (lp_dblUnitCharge * lp_dblQuantity * (lp_sngTime))
        End If
        
    End If
    
    If l_dblTotal < lp_dblMinimumCharge Then l_dblTotal = lp_dblMinimumCharge
    
    If lp_sngDiscount <> 0 Then
        l_dblTotal = (l_dblTotal * (100 - lp_sngDiscount)) / 100
    End If
    
    l_dblTotal = RoundToCurrency(l_dblTotal)
       
    'vat calculations
    
    Dim l_dblVAT As Double
    Dim l_dblTotalIncludingVAT As Double
    
    l_dblVAT = (l_dblTotal / 100) * lp_sngVATRate
    'l_dblVAT = Int((l_dblVAT * 100) + 0.5) / 100 'round
    
    l_dblVAT = RoundToCurrency(l_dblVAT)
    
    l_dblTotalIncludingVAT = l_dblTotal + l_dblVAT 'already rounded from above sums
    
    'Do the business with the included stock rates
    Dim l_dblStockTotal As Double, l_dblStockVAT As Double, l_dblStockTotalIncVAT As Double
    If l_strStockNominalCode <> "" Then
    
        l_dblStockTotal = l_dblStockUnitCharge * lp_dblQuantity
        l_dblStockTotal = (l_dblStockTotal * (100 - lp_sngDiscount)) / 100
        l_dblStockTotal = RoundToCurrency(l_dblStockTotal)
        
        l_dblStockVAT = (l_dblStockTotal / 100) * lp_sngVATRate
        l_dblStockVAT = RoundToCurrency(l_dblStockVAT)
        
        l_dblStockTotalIncVAT = l_dblStockTotal + l_dblStockVAT 'already rounded from above sums
    
    Else
    
        l_dblStockTotal = 0
        l_dblStockVAT = 0
        l_dblStockTotalIncVAT = 0
        
    End If
    
    Dim l_dblInflatedUnitCharge As Double
    Dim l_dblInflatedTotal As Double
    Dim l_dblInflatedVAT As Double
    Dim l_dblInflatedTotalIncludingVAT As Double
    
    Dim l_dblRefundedUnitCharge As Double
    Dim l_dblRefundedTotal As Double
    Dim l_dblRefundedVAT As Double
    Dim l_dblRefundedTotalIncludingVAT As Double
    
    'check for two tier pricing and calculate amounts
    
    If lp_blnTwoTier = True Then
        

        'inflated prices
        Dim l_lngCompanyID As Long
        l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
        
        l_dblInflatedUnitCharge = GetUnitCharge(g_intBaseRateCard, lp_strCETACostCode, l_lngCompanyID)
            
        If l_strUnit = "M" Then
            l_dblInflatedTotal = (l_dblInflatedUnitCharge * lp_dblQuantity * (l_sngTime2))
        ElseIf g_optDontMakeMachineTimeADecimal = 1 Then
            l_dblInflatedTotal = (lp_dblQuantity * l_dblInflatedUnitCharge / 60 * l_sngTime2)
        Else
            l_dblInflatedTotal = (lp_dblQuantity * l_dblInflatedUnitCharge * l_sngTime2)
        End If
        
        If l_dblInflatedTotal < lp_dblMinimumCharge Then l_dblInflatedTotal = lp_dblMinimumCharge
        
        l_dblInflatedTotal = RoundToCurrency(l_dblInflatedTotal)
           
        l_dblInflatedVAT = l_dblInflatedTotal * ((lp_sngVATRate / 100))
        l_dblInflatedVAT = RoundToCurrency(l_dblInflatedVAT)
        
        l_dblInflatedTotalIncludingVAT = l_dblInflatedTotal + l_dblInflatedVAT
    
        'refunded prices
        l_dblRefundedUnitCharge = l_dblInflatedUnitCharge - lp_dblUnitCharge
        l_dblRefundedTotal = l_dblInflatedTotal - l_dblTotal
        l_dblRefundedVAT = l_dblInflatedVAT - l_dblVAT
        l_dblRefundedTotalIncludingVAT = l_dblInflatedTotalIncludingVAT - l_dblTotalIncludingVAT

    End If
    
    Dim l_strIDFieldName As String
    
    If lp_strCopyType = "R" Then
        l_strIDFieldName = "resourcescheduleID"
    Else
        l_strIDFieldName = "jobdetailID"
    End If
    
    Dim l_strRateCardCategory As String
    l_strRateCardCategory = GetData("ratecard", "category", "cetacode", lp_strCETACostCode)
    Dim l_lngRateCardOrderBy As Long
    l_lngRateCardOrderBy = GetData("ratecard", "fd_orderby", "cetacode", lp_strCETACostCode)
    
    If lp_strCopyType = "O" Then lp_sngTime = 0
    
    If lp_lngCostingDetailID <> 0 Then
        'edit an existing line
        l_strSQL = "UPDATE costing SET "
        l_strSQL = l_strSQL & l_strIDFieldName & " = '" & lp_lngItemID & "', "
        l_strSQL = l_strSQL & "jobID = '" & lp_lngJobID & "', "
        l_strSQL = l_strSQL & "copytype = '" & lp_strCopyType & "', "
        l_strSQL = l_strSQL & "description = '" & l_strDescription & "', "
        l_strSQL = l_strSQL & "accountcode = '" & l_strAccountCode & "', "
        l_strSQL = l_strSQL & "costcode = '" & lp_strCETACostCode & "', "
        l_strSQL = l_strSQL & "quantity = '" & lp_dblQuantity & "', "
        l_strSQL = l_strSQL & "fd_time = '" & lp_sngTime & "', "
        l_strSQL = l_strSQL & "unit = '" & l_strUnit & "', "
        l_strSQL = l_strSQL & "unitcharge = '" & lp_dblUnitCharge & "', "
        l_strSQL = l_strSQL & "discount = '" & lp_sngDiscount & "', "
        l_strSQL = l_strSQL & "total = '" & l_dblTotal & "', "
        l_strSQL = l_strSQL & "vat = '" & l_dblVAT & "', "
        l_strSQL = l_strSQL & "totalincludingvat = '" & l_dblTotalIncludingVAT & "', "
        If lp_strCopyType <> "IS" Then
            l_strSQL = l_strSQL & "includedstocknominal = '" & l_strStockNominalCode & "', "
            l_strSQL = l_strSQL & "includedstocktotal = '" & l_dblStockTotal & "', "
            l_strSQL = l_strSQL & "includedstockvat = '" & l_dblStockVAT & "', "
            l_strSQL = l_strSQL & "includedstocktotalincludingvat = '" & l_dblStockTotalIncVAT & "', "
        Else
            l_strSQL = l_strSQL & "includedstocknominal = NULL, "
            l_strSQL = l_strSQL & "includedstocktotal = 0, "
            l_strSQL = l_strSQL & "includedstockvat = 0, "
            l_strSQL = l_strSQL & "includedstocktotalincludingvat = 0, "
        End If
        l_strSQL = l_strSQL & "inflatedunitcharge = '" & l_dblInflatedUnitCharge & "', "
        l_strSQL = l_strSQL & "inflatedtotal = '" & l_dblInflatedTotal & "', "
        l_strSQL = l_strSQL & "inflatedvat = '" & l_dblInflatedVAT & "', "
        l_strSQL = l_strSQL & "inflatedtotalincludingvat = '" & l_dblInflatedTotalIncludingVAT & "', "
        l_strSQL = l_strSQL & "refundedunitcharge = '" & l_dblRefundedUnitCharge & "', "
        l_strSQL = l_strSQL & "refundedtotal = '" & l_dblRefundedTotal & "', "
        l_strSQL = l_strSQL & "refundedvat = '" & l_dblRefundedVAT & "', "
        l_strSQL = l_strSQL & "refundedtotalincludingvat = '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & "ratecardprice = '" & lp_dblRateCardPrice & "',"
        l_strSQL = l_strSQL & "ratecardtotal = '" & l_dblRateCardTotal & "',"
        l_strSQL = l_strSQL & "ratecardcategory = '" & l_strRateCardCategory & "',"
        l_strSQL = l_strSQL & "ratecardorderby = '" & l_lngRateCardOrderBy & "',"
        l_strSQL = l_strSQL & "minimumcharge = '" & lp_dblMinimumCharge & "',"
        l_strSQL = l_strSQL & "ratecardminimumcharge = '" & lp_dblRatecardMinimumCharge & "'"
        l_strSQL = l_strSQL & " WHERE costingID = '" & lp_lngCostingDetailID & "';"
    
        cnn.Execute l_strSQL
        
    ElseIf lp_strCopyType <> "IS" Then
        'add the new line
        
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, includedstocknominal, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " prioritycode, includedstockprioritycode,"
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, includedstocktotal, includedstockvat, includedstocktotalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0','" & lp_strCopyType & "','" & l_strAccountCode & "','" & l_strStockNominalCode & "','" & lp_strCETACostCode & "','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityCode & "', '" & l_strPriorityStockCode & "',"
        l_strSQL = l_strSQL & " '" & lp_dblUnitCharge & "','" & lp_sngDiscount & "','" & l_dblTotal & "', '" & l_strUnit & "','" & l_dblVAT & "','" & l_dblTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblStockTotal & "', '" & l_dblStockVAT & "', '" & l_dblStockTotalIncVAT & "', '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
            
        cnn.Execute l_strSQL
        
    Else
    
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " prioritycode, "
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0','" & lp_strCopyType & "','" & l_strAccountCode & "','" & lp_strCETACostCode & "','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityCode & "',"
        l_strSQL = l_strSQL & " '" & lp_dblUnitCharge & "','" & lp_sngDiscount & "','" & l_dblTotal & "', '" & l_strUnit & "','" & l_dblVAT & "','" & l_dblTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
        
        cnn.Execute l_strSQL
    
        l_strSQL = "INSERT INTO costing"
        l_strSQL = l_strSQL & " (jobID, jobdetailID, costingsheetID, copytype, accountcode, costcode, description, quantity, fd_time,"
        l_strSQL = l_strSQL & " prioritycode, "
        l_strSQL = l_strSQL & " unitcharge, discount, total, unit, vat, totalincludingvat, inflatedunitcharge, inflatedtotal, inflatedvat,"
        l_strSQL = l_strSQL & " inflatedtotalincludingvat, refundedunitcharge, refundedtotal, refundedvat, refundedtotalincludingvat,"
        l_strSQL = l_strSQL & " creditnotenumber, originalcostingID, invoicenumber, page, agg, adjust, notes, category, qry, fd_order, resourcescheduleID,"
        l_strSQL = l_strSQL & " includedindealprice, ratecardprice, ratecardtotal, ratecardcategory, ratecardorderby, minimumcharge, ratecardminimumcharge)"
        l_strSQL = l_strSQL & " Values"
        l_strSQL = l_strSQL & " ('" & lp_lngJobID & "', '" & lp_lngItemID & "', '0', 'S','" & l_strStockNominalCode & "','STOCK','" & l_strDescription & "','" & lp_dblQuantity & "','" & lp_sngTime & "',"
        l_strSQL = l_strSQL & " '" & l_strPriorityStockCode & "',"
        l_strSQL = l_strSQL & " '" & l_dblStockUnitCharge & "','" & lp_sngDiscount & "','" & l_dblStockTotal & "', '', '" & l_dblStockVAT & "','" & l_dblStockTotalIncVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedUnitCharge & "', '" & l_dblInflatedTotal & "','" & l_dblInflatedVAT & "',"
        l_strSQL = l_strSQL & " '" & l_dblInflatedTotalIncludingVAT & "', '" & l_dblRefundedUnitCharge & "', '" & l_dblRefundedTotal & "', '" & l_dblRefundedVAT & "', '" & l_dblRefundedTotalIncludingVAT & "',"
        l_strSQL = l_strSQL & " Null, Null, Null, Null, Null, Null, Null, Null, Null, Null, '" & lp_lngItemID & "',"
        l_strSQL = l_strSQL & " Null, '" & lp_dblRateCardPrice & "','" & l_dblRateCardTotal & "','" & l_strRateCardCategory & "','" & l_lngRateCardOrderBy & "','" & lp_dblMinimumCharge & "','" & lp_dblRatecardMinimumCharge & "');"
                        
        cnn.Execute l_strSQL
        
    End If
        
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetUnitCharge(lp_intRateCard As Integer, lp_strFormat As String, lp_lngCompanyID As Long) As Double
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strSQL As String
    Dim l_rsRatecards As New ADODB.Recordset
    Dim l_blnFoundPrice As Boolean
    
    l_blnFoundPrice = False
    
    GetUnitCharge = 0
    
    If lp_lngCompanyID = 0 Then
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and companyID = 0"
    Else
        l_strSQL = "Select * FROM ratecard WHERE cetacode = '" & QuoteSanitise(lp_strFormat) & "' and (companyID = 0 or companyID = '" & lp_lngCompanyID & "')"
    End If
    
    l_rsRatecards.Open l_strSQL, cnn, 3, 3
    
    If l_rsRatecards.EOF <> True Then
        l_rsRatecards.MoveFirst
    End If
    
    Do While Not l_rsRatecards.EOF
        
        If l_rsRatecards("companyID") = 0 And l_blnFoundPrice = False Then
            GetUnitCharge = Val(Format(l_rsRatecards("Price" & lp_intRateCard)))
        End If
        If l_rsRatecards("companyID") = lp_lngCompanyID Then
            GetUnitCharge = Val(Format(l_rsRatecards("Price" & lp_intRateCard)))
            l_blnFoundPrice = True
            Exit Do
        End If
        
        l_rsRatecards.MoveNext
        
    Loop
    
    l_rsRatecards.Close
    Set l_rsRatecards = Nothing
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function RoundToCurrency(ByVal lp_dblAmountToRound As Double) As Double

Dim l_dblAmount As Double

If g_optRoundCurrencyDecimals <= 2 Then
    l_dblAmount = Int((lp_dblAmountToRound * 100) + 0.5) / 100
Else
    l_dblAmount = lp_dblAmountToRound
End If

RoundToCurrency = Round(l_dblAmount, g_optRoundCurrencyDecimals)

End Function

