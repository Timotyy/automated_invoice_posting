Attribute VB_Name = "VDMS_Invoice_Posting"
Option Explicit
Dim g_strConnection As String

Const g_strEmailFooter = "Content Delivery at VDMS Acton"
Public g_strUploadInvoiceLocation As String
Public g_strUploadPDFLocation As String

Public g_strAdministratorEmailAddress As String
Public g_strAdministratorEmailName As String
Public g_strBookingsEmailAddress As String
Public g_strBookingsEmailName As String

Public Const g_strBatchPrefix = "J"

Public cnn As ADODB.Connection
Public l_strCommandLine As String

Public g_intWebInvoicing As Integer
Public g_intPostToPriority As Integer

Public m_blnPostingToAccounts As Boolean

'used when checking if job is safe to send to accounts
Enum JobSafeForPostingLevel
    vbuJobSafe = -1
    vbuJobNotSafeCostingRows = 2
    vbuJobNotSafeCompanyAccountCode = 3
    vbuJobNotSafeNoPONumber = 4
    vbuJobNotSafePOTooLong = 5
    vbuJobNotSafeSAPcode = 6
End Enum

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long

Private Declare Function WNetGetConnection Lib _
   "mpr.dll" Alias "WNetGetConnectionA" (ByVal lpszLocalName _
   As String, ByVal lpszRemoteName As String, _
   cbRemoteName As Long) As Long

Private Const MAX_COMPUTERNAME_LENGTH As Long = 15&

' Reg Key ROOT Types...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Unicode nul terminated string
Const REG_DWORD = 4                      ' 32-bit number

Private Declare Function RegOpenKey Lib "advapi32.dll" _
  Alias "RegOpenKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long
   
Private Declare Function RegEnumValue Lib "advapi32.dll" _
   Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex _
   As Long, ByVal lpValueName As String, lpcbValueName _
   As Long, ByVal lpReserved As Long, lpType As Long, _
   ByVal lpData As String, lpcbData As Long) As Long
   
Private Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hKey As Long) As Long
  
Sub Main()

Dim l_strWorkstationprops As String

Set cnn = New ADODB.Connection

l_strCommandLine = Command()
If InStr(1, l_strCommandLine, "/testSystem", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=VIE-TEST-sql-1;Failover_Partner=VIE-TEST-sql-2;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
ElseIf InStr(1, l_strCommandLine, "/testTim", vbTextCompare) <> 0 Then
    g_strConnection = "DRIVER=SQL Server;SERVER=VIE-Z820-TIM.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
Else
    g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=vie-sql-1.jcatv.co.uk;Failover_Partner=vie-sql-2.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'    g_strConnection = "DRIVER=SQL Server;SERVER=vie-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
End If

If Len(l_strCommandLine) = 0 Then
    g_strUploadInvoiceLocation = "\\jcaserver\ceta\Invoices\J"
    g_strUploadPDFLocation = "\\jcaserver\ceta\Invoices\"
Else
    g_strUploadInvoiceLocation = "\\vie-handler-22\Ceta_Facilities_Manager\Invoices\J"
    g_strUploadPDFLocation = "\\vie-handler-22\Ceta_Facilities_Manager\Invoices\"
End If

cnn.Open g_strConnection

l_strWorkstationprops = GetData("xref", "descriptionalias", "description", CurrentMachineName)

g_intWebInvoicing = 0
g_intPostToPriority = 0

'Set up the default email varaibles for use with notification emails
SetDefaultEmailParameters
App.LogEvent "MX1 Invoice Posting Started"

'MsgBox "Report Path: " & vbCrLf & "batchreport.rpt", vbInformation
'PrintCrystalReport "batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & 6825 & "'", False

'If MsgBox("Exit", vbYesNo) = vbYes Then Exit Sub

If PostNewCustomersToAccounts = True Then
    SelectAndPostInvoices
    SelectAndPostCreditNotes
    ExportCrystalReport "MonthlyActForFinance.rpt", "", g_strUploadPDFLocation & "MonthlyActivityReoprt.xls"
    NotifyByEMail g_strUploadPDFLocation & "MonthlyActivityReoprt.xls"
Else
    SendSMTPMail g_strAdministratorEmailAddress, g_strAdministratorEmailName, "MX1 Invoice Posting Error", "Customer Posting failed", "", True, "", "", ""
End If

cnn.Close

Set cnn = Nothing
Beep
End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetDataSQL(lp_strSQL As String) As Variant

On Error GoTo Proc_GetData_Error

Dim l_rstGetData As New ADODB.Recordset
Dim l_conGetData As New ADODB.Connection

l_conGetData.Open g_strConnection
l_rstGetData.Open lp_strSQL, l_conGetData, adOpenStatic, adLockReadOnly

If Not l_rstGetData.EOF Then
    
    If Not IsNull(l_rstGetData(0)) Then
        GetDataSQL = l_rstGetData(0)
    Else
        Select Case l_rstGetData.Fields(0).Type
        Case adChar, adVarChar, adVarWChar, 201
            GetDataSQL = ""
        Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
            GetDataSQL = 0
        Case adDate, adDBDate, adDBTime, adDBTimeStamp
            GetDataSQL = 0
        Case Else
            GetDataSQL = False
        End Select
    End If

End If

l_rstGetData.Close
Set l_rstGetData = Nothing

l_conGetData.Close
Set l_conGetData = Nothing

Exit Function

Proc_GetData_Error:

GetDataSQL = ""

End Function
Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Sub SetDefaultEmailParameters()

Dim rst As New ADODB.Recordset, SQL As String

SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strAdministratorEmailName = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'BookingsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strBookingsEmailName = rst("value")
End If
rst.Close

End Sub

Sub SelectAndPostInvoices()

Dim l_strWhereSQL As String, l_strSQL As String, rst As ADODB.Recordset, l_strType As String
Dim l_lngBatchNumber As Long, l_lngCreditBatchNumber As Long, l_lngCurrentJobID As Long, l_strNotPostMsg As String

On Error GoTo PROC_ERROR_INVOICE

l_strType = "INVOICE"

'invoices
l_strWhereSQL = " (job.invoiceddate Is Not null) and (job.jobtype <> 'CREDIT') AND (job.senttoaccountsdate Is null) AND (job.cancelleddate Is null) AND (job.flagquote <> " & GetFlag(1) & ")"
l_strSQL = "SELECT * FROM job WHERE " & l_strWhereSQL & " ORDER BY invoicenumber, jobID"
m_blnPostingToAccounts = True
    
Debug.Print l_strSQL
Set rst = New ADODB.Recordset
rst.Open l_strSQL, cnn, 3, 3

If rst.RecordCount > 0 Then
    rst.MoveFirst
    
    'get the next batch number
    l_lngBatchNumber = GetNextSequence("batchnumber")
    
    App.LogEvent "Commencing Posting Invoices"
    
    Do While Not rst.EOF
    
        'pick up the current job ID
        l_lngCurrentJobID = rst("jobID")
        l_strNotPostMsg = "The job with ID " & l_lngCurrentJobID & " can not be posted as "
        DoEvents
                
        'check to see if there are any problems with this jobs costing records
        Select Case JobSafeForPosting(l_lngCurrentJobID)
        Case vbuJobSafe
                
            If SendJobToAccounts(l_lngCurrentJobID, l_strType, l_lngBatchNumber) = True Then
            
                'update the job status, but use the parameter to stop the sent to accounts date being re-set
                SetData "job", "senttoaccountsdate", "jobID", l_lngCurrentJobID, FormatSQLDate(Now)
                SetData "job", "senttoaccountsuser", "jobID", l_lngCurrentJobID, "CETA"
                SetData "job", "fd_status", "jobid", l_lngCurrentJobID, "Sent To Accounts"
                cnn.Execute "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & l_lngCurrentJobID & "', 'CETA', '" & FormatSQLDate(Now) & "', 'Sent To Accounts', GETdate())"
            Else
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            End If
                            
        Case vbuJobNotSafeNoPONumber
            'no PO number on the invoice
            NotifyProblemByEmail l_lngCurrentJobID, "There is no PO number on the invoice"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeCompanyAccountCode
            'no company account code
            NotifyProblemByEmail l_lngCurrentJobID, "There is no company account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafeCostingRows
            'problem with costing lines
            NotifyProblemByEmail l_lngCurrentJobID, "There is at least one costing line without an account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafePOTooLong
            NotifyProblemByEmail l_lngCurrentJobID, "The PO number on the invoice has more than 100 characters"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeSAPcode
            NotifyProblemByEmail l_lngCurrentJobID, "There is no SAP code against the current customer"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        End Select
        
        'move to the next job
        rst.MoveNext
    
    Loop
    
    'Reports and closedown
    App.LogEvent "Printing Batch Report " & g_strBatchPrefix & l_lngBatchNumber & " to report batchreport.rpt"
    PrintCrystalReport "batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", False
    App.LogEvent "Exporting Batch Report " & g_strBatchPrefix & l_lngBatchNumber
    ExportCrystalReport "batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    ExportCrystalReport "batchsagereport.rpt", "{batchsage.Details} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    App.LogEvent "Notifying by Email " & g_strBatchPrefix & l_lngBatchNumber
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    
End If
rst.Close
Set rst = Nothing
App.LogEvent "Completed Posting Invoices"

PROC_EXIT:
Exit Sub

PROC_ERROR_INVOICE:
SendSMTPMail g_strAdministratorEmailAddress, "", "SelectAndPostInvoices generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Sub

Sub SelectAndPostCreditNotes()

Dim l_strWhereSQL As String, l_strSQL As String, rst As ADODB.Recordset, l_strType As String
Dim l_lngBatchNumber As Long, l_lngCreditBatchNumber As Long, l_lngCurrentJobID As Long, l_strNotPostMsg As String

On Error GoTo PROC_ERROR_CREDIT

l_strType = "CREDIT"

'credit notes
l_strWhereSQL = "(jobtype='CREDIT' AND job.creditnotedate IS NOT NULL AND (creditsenttoaccountsdate IS NULL) AND cancelleddate IS NULL AND flagquote <> " & GetFlag(1) & ")"
l_strSQL = "SELECT jobID, creditnotenumber as invoicenumber, creditsenttoaccountsdate as senttoaccountsdate, creditsenttoaccountsuser as senttoaccountsuser, companyname, creditnotedate as invoiceddate, deadlinedate, createddate, createduser, companyID, contactname, contactID, telephone, email, fax, orderreference, title1, title2, startdate FROM job WHERE " & l_strWhereSQL & " ORDER BY creditnotenumber, jobID"
m_blnPostingToAccounts = True

Set rst = New ADODB.Recordset
rst.Open l_strSQL, cnn, 3, 3

If rst.RecordCount > 0 Then
    rst.MoveFirst
        
    'get the next batch number
    l_lngBatchNumber = GetNextSequence("batchnumber")
    
    App.LogEvent "Commencing Posting Credit Notes"
    
    Do While Not rst.EOF
        
        'pick up the current job ID
        l_lngCurrentJobID = rst("jobID")
        l_strNotPostMsg = "The job with ID " & l_lngCurrentJobID & " can not be posted as "
        DoEvents
                
        'check to see if there are any problems with this jobs costing records
        Select Case JobSafeForPosting(l_lngCurrentJobID)
        Case vbuJobSafe
                
            If SendJobToAccounts(l_lngCurrentJobID, l_strType, l_lngBatchNumber) = True Then
            
                'update the job status, but use the parameter to stop the sent to accounts date being re-set
                SetData "job", "senttoaccountsdate", "jobID", l_lngCurrentJobID, FormatSQLDate(Now)
                SetData "job", "senttoaccountsuser", "jobID", l_lngCurrentJobID, "CETA"
                SetData "job", "fd_status", "jobid", l_lngCurrentJobID, "Sent To Accounts"
                cnn.Execute "INSERT INTO jobhistory (jobID, cuser, cdate, description, servertime) VALUES ('" & l_lngCurrentJobID & "', 'CETA', '" & FormatSQLDate(Now) & "', 'Sent To Accounts', GETdate())"
            Else
                ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            End If
            
        Case vbuJobNotSafeNoPONumber
            'no PO number on the invoice
            NotifyProblemByEmail l_lngCurrentJobID, "There is no PO number on the invoice"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeCompanyAccountCode
            'no company account code
            NotifyProblemByEmail l_lngCurrentJobID, "There is no company account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafeCostingRows
            'problem with costing lines
            NotifyProblemByEmail l_lngCurrentJobID, "There is at least one costing line without an account code"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
            
        Case vbuJobNotSafePOTooLong
            NotifyProblemByEmail l_lngCurrentJobID, "The PO number on the invoice has more than 100 characters"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        
        Case vbuJobNotSafeSAPcode
            NotifyProblemByEmail l_lngCurrentJobID, "There is no SAP code against the current customer"
            ResetSentToAccountsOnJob l_lngCurrentJobID, l_strType
        End Select
        
        'move to the next job
        rst.MoveNext
    
    Loop
    
    'Reports and closedown
    App.LogEvent "Printing Batch Report " & g_strBatchPrefix & l_lngBatchNumber & " to report batchreport.rpt"
    PrintCrystalReport "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", False
    App.LogEvent "Exporting Batch Report " & g_strBatchPrefix & l_lngBatchNumber
    ExportCrystalReport "Batchreport.rpt", "{batchjob.batch_number} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    ExportCrystalReport "Batchsagereport.rpt", "{batchsage.Details} = '" & g_strBatchPrefix & l_lngBatchNumber & "'", g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    App.LogEvent "Notifying by Email " & g_strBatchPrefix & l_lngBatchNumber
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & "_sage.xls"
    
End If
rst.Close
Set rst = Nothing
App.LogEvent "Completed Posting Credit Notes"

PROC_EXIT:
Exit Sub

PROC_ERROR_CREDIT:
SendSMTPMail g_strAdministratorEmailAddress, "", "SelectAndPostCreditNotes generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Sub

Function PostNewCustomersToAccounts() As Boolean
    
Dim l_rstNewCustomers As New ADODB.Recordset
Dim l_strSQL As String, l_lngBatchNumber As Long
Dim cnn2 As ADODB.Connection, rst As ADODB.Recordset
Dim ACCNUMBER As String

l_strSQL = "SELECT * FROM company WHERE postedtoaccountsdate IS NULL AND iscustomer = 1 and system_active <> 0;"
l_rstNewCustomers.Open l_strSQL, cnn, 3, 3

l_lngBatchNumber = GetNextSequence("batchnumber")

'On Error GoTo PROC_ERROR_CUSTOMERS

If Not l_rstNewCustomers.EOF Then
    l_rstNewCustomers.MoveFirst
    While Not l_rstNewCustomers.EOF
        If Trim(" " & l_rstNewCustomers("salesledgercode")) = "" Then
            PostNewCustomersToAccounts = False
            l_rstNewCustomers.Close
            Set l_rstNewCustomers = Nothing
            Exit Function
        End If
        App.LogEvent "Posting New Customer " & l_rstNewCustomers("salesledgercode")
        ' Do the traditional posting anyway
        l_strSQL = "INSERT INTO batchcompany (batchnumber, code, clientname, address1, postcode, telephone, fax, vatnumber, ecvatstatus, eccountry, ecvat, ratecard, ready, clientnumber, BusinessUnit, email) VALUES ("
        l_strSQL = l_strSQL & l_lngBatchNumber & ", "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("salesledgercode") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("name")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstNewCustomers("address")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("postcode") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("telephone") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("fax") & "', "
        Select Case l_rstNewCustomers("vatcode")
            Case "G"
                l_strSQL = l_strSQL & "'" & Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2) & "', "
                l_strSQL = l_strSQL & "'G', "
                l_strSQL = l_strSQL & "'" & Left(l_rstNewCustomers("vatnumber"), 2) & "', "
                l_strSQL = l_strSQL & "'E', "
            Case "7"
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'7', "
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'O', "
            Case Else
                l_strSQL = l_strSQL & "'', "
                l_strSQL = l_strSQL & "'1', "
                l_strSQL = l_strSQL & "'GB', "
                l_strSQL = l_strSQL & "'U', "
        End Select
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("ratecard") & "', "
        l_strSQL = l_strSQL & "'READY', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("companyID") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("BusinessUnit") & "', "
        l_strSQL = l_strSQL & "'" & l_rstNewCustomers("email") & "') "
        
        cnn.Execute l_strSQL
            
        If g_intPostToPriority <> 0 Then
        
            'Then also do the priority posting
            Set cnn2 = New ADODB.Connection
            cnn2.ConnectionString = "DRIVER=SQL Server;SERVER=172.19.4.232;DATABASE=rreur;UID=RReuropeSQL;PWD=Re1234"
            cnn2.Open
            
            Set rst = cnn2.Execute("SELECT ACCNAME FROM HSLOADACCOUNTS WHERE ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';")
            If rst.EOF Then
                rst.Close
                l_strSQL = "INSERT INTO HSLOADACCOUNTS (TYPE, DNAME, ACCNAME, ACCDES, ADDRESS, ZIP, PHONE, FAX, VATNUM, SPEC1, COUNTRYNAME, SPEC3, SPEC6, DETAILS, EMAIL) VALUES ('R', 'CETA', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("salesledgercode")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(QuoteSanitise(StrReverse(l_rstNewCustomers("name")))) & "', "
                l_strSQL = l_strSQL & "'" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("postcode")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("telephone")) & "', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("fax")) & "', "
                Select Case l_rstNewCustomers("vatcode")
                    Case "G"
                        l_strSQL = l_strSQL & "'" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                        l_strSQL = l_strSQL & "'000', "
                        l_strSQL = l_strSQL & "'" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                        l_strSQL = l_strSQL & "'E', "
                    Case "7"
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'800', "
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'O', "
                    Case Else
                        l_strSQL = l_strSQL & "'', "
                        l_strSQL = l_strSQL & "'100', "
                        l_strSQL = l_strSQL & "'GB', "
                        l_strSQL = l_strSQL & "'U', "
                End Select
                Select Case Trim(" " & l_rstNewCustomers("Businessunit"))
                    Case "S&E"
                        l_strSQL = l_strSQL & "'" & StrReverse("OU") & "', "
                    Case "TVI"
                        l_strSQL = l_strSQL & "'" & StrReverse("24/7") & "', "
                    Case Else
                        l_strSQL = l_strSQL & "'" & StrReverse(Trim(" " & l_rstNewCustomers("Businessunit"))) & "', "
                End Select
                l_strSQL = l_strSQL & "'READY', "
                l_strSQL = l_strSQL & "'" & Trim(l_rstNewCustomers("email")) & "') "
                
            Else
                ACCNUMBER = rst("ACCNAME")
                l_strSQL = "UPDATE HSLOADACCOUNTS SET ACCNAME = '" & ACCNUMBER & "', ORIGACCNAME = '" & l_rstNewCustomers("salesledgercode") & "' WHERE ACCNAME = '" & l_rstNewCustomers("salesledgercode") & "';"
                Debug.Print l_strSQL
                cnn.Execute l_strSQL
                rst.Close
                l_strSQL = "UPDATE HSLOADACCOUNTS SET "
                l_strSQL = l_strSQL & "ADDRESS = '" & Right(Trim(QuoteSanitise(StrReverse(Replace(l_rstNewCustomers("address"), vbNewLine, " ")))), 120) & "', "
                l_strSQL = l_strSQL & "ZIP = '" & Trim(QuoteSanitise(l_rstNewCustomers("postcode"))) & "', "
                l_strSQL = l_strSQL & "PHONE = '" & Trim(QuoteSanitise(l_rstNewCustomers("telephone"))) & "', "
                l_strSQL = l_strSQL & "FAX = '" & Trim(QuoteSanitise(l_rstNewCustomers("fax"))) & "', "
                Select Case l_rstNewCustomers("vatcode")
                    Case "G"
                        l_strSQL = l_strSQL & "VATNUM = '" & Trim(Mid(l_rstNewCustomers("vatnumber"), 3, Len(l_rstNewCustomers("vatnumber")) - 2)) & "', "
                        l_strSQL = l_strSQL & "SPEC1 = '000', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = '" & Trim(Left(l_rstNewCustomers("vatnumber"), 2)) & "', "
                        l_strSQL = l_strSQL & "SPEC3 = 'E', "
                    Case "7"
                        l_strSQL = l_strSQL & "VATNUM = '', "
                        l_strSQL = l_strSQL & "SPEC1 = '800', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = '', "
                        l_strSQL = l_strSQL & "SPEC3 = 'O', "
                    Case Else
                        l_strSQL = l_strSQL & "VATNUM = '', "
                        l_strSQL = l_strSQL & "SPEC1 = '100', "
                        l_strSQL = l_strSQL & "COUNTRYNAME = 'GB', "
                        l_strSQL = l_strSQL & "SPEC3 = 'U', "
                End Select
                l_strSQL = l_strSQL & "SPEC6 = '" & StrReverse(Trim(" " & l_rstNewCustomers("BusinessUnit"))) & "', "
                l_strSQL = l_strSQL & "DETAILS = 'READY', "
                l_strSQL = l_strSQL & "EMAIL = '" & l_rstNewCustomers("email") & "', "
                l_strSQL = l_strSQL & "CUSTLOADED = '', "
                l_strSQL = l_strSQL & "SPECLOADED = '' "
                l_strSQL = l_strSQL & "WHERE ACCNAME = '" & ACCNUMBER & "';"

            End If
            
            Debug.Print l_strSQL
            
            'Post it to our local copy of the table for checking purposes
            cnn.Execute l_strSQL
            'Then post it to Israel and close that connection
            cnn2.Execute l_strSQL
            cnn2.Close
            
            Set rst = Nothing
            Set cnn2 = Nothing
            
        End If
        
        SetData "company", "postedtoaccountsdate", "companyID", l_rstNewCustomers("companyID"), Format(Now, "MM/DD/YYYY")
        l_rstNewCustomers.MoveNext
    Wend
    'Print a report of what we've done
    PrintCrystalReport "BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, False
    ExportCrystalReport "BatchCustomer.rpt", "{batchcompany.batchnumber} = " & l_lngBatchNumber, g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    NotifyByEMail g_strUploadInvoiceLocation & l_lngBatchNumber & ".xls"
    
End If

l_rstNewCustomers.Close
Set l_rstNewCustomers = Nothing

PostNewCustomersToAccounts = True
App.LogEvent "Completed Posting New Customers"

PROC_EXIT:
Exit Function

PROC_ERROR_CUSTOMERS:
PostNewCustomersToAccounts = False
SendSMTPMail g_strAdministratorEmailAddress, "", "PostNewCustomersToAccounts generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""

Resume PROC_EXIT

End Function

Function GetNextSequence(lp_strSequenceName As String) As Long
    
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Dim l_rstSequence As New ADODB.Recordset
    l_rstSequence.Open l_strSQL, cnn, 3, 3
    
    If Not l_rstSequence.EOF Then
        l_rstSequence.MoveFirst
        l_lngGetNextSequence = l_rstSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = '" & "CETA" & "', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        cnn.Execute l_strSQL
    End If
    
    l_rstSequence.Close
    Set l_rstSequence = Nothing
    
    GetNextSequence = l_lngGetNextSequence
        
End Function

Function JobSafeForPosting(lp_lngJobID As Long) As JobSafeForPostingLevel
    
    Dim l_strSQL As String
    
    'check the job for the client, and the client account code
    Dim l_strCompanyAccountCode As String
    If g_intWebInvoicing = 1 Then
        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("accountstransaction", "companyID", "accountstransactionID", lp_lngJobID))
    Else
        l_strCompanyAccountCode = GetData("company", "salesledgercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
    End If
    
    If l_strCompanyAccountCode = "" Then
        JobSafeForPosting = vbuJobNotSafeCompanyAccountCode
        Exit Function
    End If
    
    'Check SAP code
    Dim l_strSAPcustomercode As String
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", GetData("job", "companyID", "jobID", lp_lngJobID))
    If l_strCompanyAccountCode = "" Then
        JobSafeForPosting = vbuJobNotSafeSAPcode
        Exit Function
    End If
    
    'Check PO number
    Dim l_strOrderNumber As String
    l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
    If Len(l_strOrderNumber) > 100 Then
        JobSafeForPosting = vbuJobNotSafePOTooLong
        Exit Function
    End If
    
    If g_intWebInvoicing = 1 Then
        'Check there is a PO number
        If GetData("accountstransaction", "clientponumber", "accountstransactionID", lp_lngJobID) = "" Then
            JobSafeForPosting = vbuJobNotSafeNoPONumber
            Exit Function
        End If
    End If
    
    'check the costing lines for valid account codes
    If g_intWebInvoicing = 1 Then
        l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE accountstransactionID = '" & lp_lngJobID & "' AND (accountcode IS NULL OR accountcode = '')"
    Else
        l_strSQL = "SELECT COUNT(costingID) FROM costing WHERE jobID = '" & lp_lngJobID & "' AND (accountcode IS NULL OR accountcode = '')"
    End If
    Dim l_intInvalidCostingLines As Integer
    l_intInvalidCostingLines = GetCount(l_strSQL)
    
    If l_intInvalidCostingLines > 0 Then
        JobSafeForPosting = vbuJobNotSafeCostingRows
    Else
        JobSafeForPosting = vbuJobSafe
    End If
    
End Function

Function SendJobToAccounts(lp_lngJobID As Long, lp_strType As String, lp_lngBatchNumber As Long) As Boolean

'declare all required variables
Dim l_strCompanyAccountNumber As String, l_lngInvoiceNumber As Long, l_datInvoiceDate As Date, l_datInvoicePayDate As Date, l_strOrderNumber As String, l_strVATCode As String, l_strPriorityCode As String
Dim l_curTotal As Currency, l_curVAT As Currency, l_curTotalIncVAT As Currency, l_strItemAccountCode As String, l_strGeneralNotes As String, l_strCetaCode As String, l_strCetaStockCode As String, l_strCetaDescription As String
Dim l_strItemStockCode As String, l_strItemStockPriorityCode As String, l_curStockTotal As Currency, l_curStockVAT As Currency, l_curStockTotalIncVAT As Currency
Dim l_strCompanyName As String, l_strTheService As String, l_strVDMS_Charge_Code As String, l_strVDMS_Stock_Charge_Code As String

Dim l_lngCompanyID As Long
Dim l_strSQL As String, l_strType As String

SendJobToAccounts = True

l_strType = lp_strType 'This gets changed during the function, and the calling routine needs for the parameter it sent to not change.
App.LogEvent "Posting Job Number: " & lp_lngJobID

'get the company ID as it is needed to work out other values
l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
l_strCompanyName = GetData("job", "companyname", "jobID", lp_lngJobID)

'get the company account number from company table - needs to be the salesledger code, not thew normal company code
l_strCompanyAccountNumber = GetData("company", "salesledgercode", "companyID", l_lngCompanyID)

'is this an invoice or a credit, as it makes a difference
Select Case lp_strType
Case "INVOICE"
    l_strType = "SI"
    l_lngInvoiceNumber = GetData("job", "invoicenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "invoiceddate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE ((creditnotenumber IS NULL) OR (creditnotenumber = 0)) AND (jobID = '" & lp_lngJobID & "') ORDER BY costingID"
Case "CREDIT"
    l_strType = "SC"
    l_lngInvoiceNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
    l_datInvoiceDate = GetData("job", "creditnotedate", "jobID", lp_lngJobID)
    l_strSQL = "SELECT * FROM costing WHERE jobID = '" & lp_lngJobID & "'"
End Select

'add 30 days to invoice date for pay date
l_datInvoiceDate = Format(l_datInvoiceDate, "dd/mm/yyyy")
l_datInvoicePayDate = DateAdd("d", 30, l_datInvoiceDate)

'order number - if this is blank then send the customer contactname for the job

l_strOrderNumber = GetData("job", "orderreference", "jobID", lp_lngJobID)
If l_strOrderNumber = "" Then l_strOrderNumber = GetData("job", "contactname", "jobID", lp_lngJobID)
'vat code
l_strVATCode = GetData("company", "vatcode", "companyID", l_lngCompanyID)
If l_strVATCode = "" Then l_strVATCode = "1"

Select Case l_strVATCode
    Case "", "1"
        l_strVATCode = "T1"
    Case "G"
        l_strVATCode = "T4"
    Case "7"
        l_strVATCode = "T0"
End Select

'general notes
l_strGeneralNotes = GetData("job", "notes1", "jobID", lp_lngJobID)

'open up the costing table and loop through
Dim l_rstCosting As New ADODB.Recordset
l_rstCosting.Open l_strSQL, cnn, 3, 3

'if there are records, then move to the first one
If Not l_rstCosting.EOF Then l_rstCosting.MoveFirst

'start the loop
Do While Not l_rstCosting.EOF
    'pick up values for each line
    
    'standard fields
    If lp_strType = "INVOICE" Then
        l_curTotal = l_rstCosting("total")
        l_curVAT = l_rstCosting("vat")
        l_curTotalIncVAT = l_rstCosting("totalincludingvat")
        l_curStockTotal = Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    Else
        l_curTotal = 0 - l_rstCosting("total")
        l_curVAT = 0 - l_rstCosting("vat")
        l_curTotalIncVAT = 0 - l_rstCosting("totalincludingvat")
        l_curStockTotal = 0 - Val(Trim(" " & l_rstCosting("includedstocktotal")))
        l_curStockVAT = 0 - Val(Trim(" " & l_rstCosting("includedstockvat")))
        l_curStockTotalIncVAT = 0 - Val(Trim(" " & l_rstCosting("includedstocktotalincludingvat")))
    End If
    
    l_strItemAccountCode = l_rstCosting("accountcode")
    l_strItemStockCode = Trim(" " & l_rstCosting("includedstocknominal"))
    l_strTheService = Trim(GetData("jobdetail", "workflowdescription", "jobdetailID", l_rstCosting("jobdetailID")))
    
    l_strCetaCode = l_rstCosting("costcode")
    l_strCetaDescription = Trim(" " & l_rstCosting("description"))
    If l_strItemStockCode <> "" Then l_strCetaStockCode = GetData("ratecard", "cetacode", "nominalcode", l_strItemStockCode)
    l_strPriorityCode = Trim(" " & l_rstCosting("prioritycode"))
    l_strItemStockPriorityCode = Trim(" " & l_rstCosting("includedstockprioritycode"))
    If l_strItemStockPriorityCode <> "" Then Trim (" " & l_strItemStockPriorityCode = GetData("ratecard", "cetacode", "prioritycode", l_strItemStockCode))
    l_strVDMS_Charge_Code = l_rstCosting("VDMS_Charge_Code")
    l_strVDMS_Stock_Charge_Code = Trim(" " & l_rstCosting("includedstock_VDMS_Charge_Code"))
    
    'create a new record in the batch table
    If AddLineToBatch(lp_lngJobID, l_strType, l_strCompanyAccountNumber, l_strCompanyName, l_lngInvoiceNumber, l_datInvoiceDate, l_datInvoicePayDate, l_strOrderNumber, l_strVATCode, l_curTotal, l_curVAT, l_curTotalIncVAT, l_strItemAccountCode, l_strCetaCode, l_strCetaDescription, l_strGeneralNotes, lp_lngBatchNumber, l_strItemStockCode, l_strCetaStockCode, l_curStockTotal, l_curStockVAT, l_curStockTotalIncVAT, l_strPriorityCode, l_strItemStockPriorityCode, l_strTheService, l_strVDMS_Charge_Code, l_strVDMS_Stock_Charge_Code) = False Then
        SendJobToAccounts = False
        Exit Function
    End If
    
    'move on to the next costing record
    l_rstCosting.MoveNext
Loop

'close the recordset
l_rstCosting.Close
Set l_rstCosting = Nothing

'is this a credit or an invoice? update the correct fields

'set the job and invoice header tables to include the batch number

Select Case lp_strType
Case "INVOICE"
    SetData "job", "senttoaccountsbatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & "CETA" & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'INVOICE'"

Case "CREDIT"
    SetData "job", "creditnotebatchnumber", "jobID", lp_lngJobID, lp_lngBatchNumber
    SetData "job", "creditsenttoaccountsdate", "jobID", lp_lngJobID, FormatSQLDate(Now)
    SetData "job", "creditsenttoaccountsuser", "jobID", lp_lngJobID, "CETA"
    l_strSQL = "UPDATE invoiceheader SET batchnumber = '" & lp_lngBatchNumber & "', senttoaccountsdate = '" & FormatSQLDate(Now) & "', senttoaccountsuser ='" & "CETA" & "' WHERE jobID = '" & lp_lngJobID & "' AND invoicetype = 'CREDIT'"
End Select

'update the rows
cnn.Execute l_strSQL

End Function

Function AddLineToBatch(lp_lngJobID As Long, lp_strType As String, lp_strCompanyAccountCode As String, lp_strCompanyName As String, lp_lngInvoiceNumber As Long, _
                            lp_datInvoiceDate As Date, lp_datInvoicePayDate As Date, lp_strOrderNumber As String, lp_strVATCode As String, _
                            lp_curTotal As Currency, lp_curVAT As Currency, lp_curTotalIncVAT As Currency, lp_strAccountCode As String, lp_strCETACode As String, lp_strCetaDescription As String, _
                            lp_strGeneralNotes As String, lp_lngBatchNumber As Long, Optional lp_strStockCode As String, Optional lp_strCetaStockCode As String, _
                            Optional lp_curStockTotal As Currency, Optional lp_curStockVAT As Currency, Optional lp_curStockTotalIncVAT As Currency, Optional lp_strPriorityCode As String, _
                            Optional lp_strItemStockPriorityCode As String, Optional lp_strTheService As String, Optional lp_strVDMS_Charge_Code As String, _
                            Optional lp_strVDMS_Stock_Charge_Code As String) As Boolean

If lp_curTotalIncVAT <> lp_curTotal + lp_curVAT Then
    AddLineToBatch = False
    Exit Function
End If

Dim l_rstBatchJob As New ADODB.Recordset
Dim l_strNewString As String
Dim l_strSQL As String
Dim l_strPriorityCode As String, l_strPriorityStockCode As String, l_strSAPcustomercode As String
Dim l_lngCompanyID As Long, l_strBusinessUnit As String
Dim l_strSageCustomerReference As String

On Error GoTo PROC_ERROR_LINE

If lp_curTotal <> 0 Then
    
    'Add an item to the batchsage table, for this item.
    l_lngCompanyID = GetData("job", "companyID", "jobID", lp_lngJobID)
    l_strSageCustomerReference = GetData("company", "SageCustomerReference", "companyID", l_lngCompanyID)
    l_strSAPcustomercode = GetData("company", "SAPcustomercode", "companyID", l_lngCompanyID)
    l_strBusinessUnit = GetData("company", "BusinessUnit", "companyID", l_lngCompanyID)
    
    l_strSQL = "INSERT INTO batchsage (Type, AccountReference, NominalAccountReference, ItemDate, Reference, Details, Service, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
    l_strSQL = l_strSQL & "'" & lp_strType & "', "
    l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
    l_strSQL = l_strSQL & "'" & lp_strVDMS_Charge_Code & "', "
    l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
    l_strSQL = l_strSQL & "'" & lp_lngInvoiceNumber & "CETA', "
    l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
    l_strSQL = l_strSQL & "'" & lp_strTheService & "', "
    l_strSQL = l_strSQL & Format(lp_curTotal - lp_curStockTotal, "#.00") & ", "
    l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
    l_strSQL = l_strSQL & Format(lp_curVAT - lp_curStockVAT, "#.00") & ", "
    l_strSQL = l_strSQL & "1, "
    l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
    
    Debug.Print l_strSQL
    cnn.Execute l_strSQL
    
    If lp_curStockTotal <> 0 Then
        l_strSQL = "INSERT INTO batchsage (Type, AccountReference, NominalAccountReference, ItemDate, Reference, Details, NetAmount, TaxCode, TaxAmount, ExchangeRate, ExtraReference) VALUES ("
        l_strSQL = l_strSQL & "'" & lp_strType & "', "
        l_strSQL = l_strSQL & "'" & l_strSageCustomerReference & "', "
        l_strSQL = l_strSQL & "'" & lp_strVDMS_Stock_Charge_Code & "', "
        l_strSQL = l_strSQL & "'" & Format(lp_datInvoiceDate, "YYYY-MM-DD") & "', "
        l_strSQL = l_strSQL & "'" & lp_lngInvoiceNumber & "CETA', "
        l_strSQL = l_strSQL & "'" & g_strBatchPrefix & lp_lngBatchNumber & "', "
        l_strSQL = l_strSQL & Format(lp_curStockTotal, "#.00") & ", "
        l_strSQL = l_strSQL & "'" & lp_strVATCode & "', "
        l_strSQL = l_strSQL & Format(lp_curStockVAT, "#.00") & ", "
        l_strSQL = l_strSQL & "1, "
        l_strSQL = l_strSQL & "'" & Left(QuoteSanitise(lp_strOrderNumber), 30) & "');"
        Debug.Print l_strSQL
        cnn.Execute l_strSQL
    End If
    
    'open the batchjob table with no records and do the old style posting anyway
    l_strSQL = "SELECT * FROM batchjob WHERE batchjobID = -1"
    l_rstBatchJob.Open l_strSQL, cnn, 3, 3
        
    If lp_curStockTotal <> 0 Then

        'There is an included stock component of this line - split it into two lines for accounts
        'add a line to the batch table for original line
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SageCode") = lp_strVDMS_Charge_Code
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber & "CETA"
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal - lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT - lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT - lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

        'add a line to the batch table for the stock
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber & "CETA"
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curStockTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curStockVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curStockTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strStockCode
        l_rstBatchJob("SageCode") = lp_strVDMS_Stock_Charge_Code
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    Else

        'add a line to the batch table
        l_rstBatchJob.AddNew
        l_rstBatchJob("Type") = lp_strType
        l_rstBatchJob("Customer_Account_Code") = lp_strCompanyAccountCode
        l_rstBatchJob("Customer_Name") = lp_strCompanyName
        l_rstBatchJob("SAPcustomercode") = l_strSAPcustomercode
        l_rstBatchJob("SageCustomerReference") = l_strSageCustomerReference
        l_rstBatchJob("Invoice_Number") = lp_lngInvoiceNumber & "CETA"
        l_rstBatchJob("Invoice_Date") = lp_datInvoiceDate
        l_rstBatchJob("Invoice_Pay_Date") = lp_datInvoicePayDate
        l_rstBatchJob("Order_Number") = lp_strOrderNumber
        l_rstBatchJob("Vat_Code") = lp_strVATCode
        l_rstBatchJob("Total") = Format(lp_curTotal, "#.00")
        l_rstBatchJob("VAT") = Format(lp_curVAT, "#.00")
        l_rstBatchJob("Total_Inc_VAT") = Format(lp_curTotalIncVAT, "#.00")
        l_rstBatchJob("Account_Code") = lp_strAccountCode
        l_rstBatchJob("SageCode") = lp_strVDMS_Charge_Code
        l_rstBatchJob("companyID") = l_lngCompanyID
        l_rstBatchJob("BusinessUnit") = l_strBusinessUnit

        ' replace carriage returns with underscores
        l_strNewString = Replace(lp_strGeneralNotes, Chr(10), "_")
        ' replace hard returns with blanks
        l_strNewString = Replace(l_strNewString, Chr(13), "")
        ' Truncate to 50 characters
        l_strNewString = Trim(Left(l_strNewString, 50))

        l_rstBatchJob("Notes") = l_strNewString
        l_rstBatchJob("Batch_Number") = g_strBatchPrefix & lp_lngBatchNumber
        l_rstBatchJob("Collected_By_Accounts") = "READY"

        'save the record
        l_rstBatchJob.Update

    End If
    
    'close the recordset
    l_rstBatchJob.Close
    Set l_rstBatchJob = Nothing
        
End If

AddLineToBatch = True

PROC_EXIT:

Exit Function

PROC_ERROR_LINE:

SendSMTPMail g_strAdministratorEmailAddress, "", "AddLineToBatch generated an error", "Error Number: " & Err.Number & vbCrLf & "Error Descrtipion: " & Err.Description & vbCrLf, "", True, "", "", ""
AddLineToBatch = False
Resume PROC_EXIT
        
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String

If IsNull(lp_varValue) Or lp_varValue = "" Or UCase(lp_varValue) = "NULL" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

cnn.Execute l_strSQL

End Function

Sub PrintCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_blnPreview As Boolean, Optional ByRef lp_lngCopies As Long, Optional ByRef lp_strPrinterToPrintTo As String)
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    'Check whether a number of copies was sent, and if not set to 1
    If lp_lngCopies = 0 Then
        lp_lngCopies = 1
    End If
    
    If lp_blnPreview = True Then lp_lngCopies = 1
    
    crxReport.DiscardSavedData
    
    If lp_strPrinterToPrintTo <> "" Then
        crxReport.PrinterSetup 0
    End If
        
    crxReport.PrintOut False, lp_lngCopies
        
    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Sub ExportCrystalReportAsPDF(ByRef lp_strRPTFileName As String, ByRef lp_strExportFileName As String)

    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    crxReport.DiscardSavedData
    
    crxReport.ExportOptions.DestinationType = crEDTDiskFile
    crxReport.ExportOptions.FormatType = crEFTPortableDocFormat
    crxReport.ExportOptions.DiskFileName = lp_strExportFileName
    crxReport.ExportOptions.ExcelUseConstantColumnWidth = False
    crxReport.ExportOptions.ExcelUseFormatInDataOnly = True
    crxReport.Export False

    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Sub ExportCrystalReport(ByRef lp_strRPTFileName As String, ByRef lp_strSelectionFormula As String, ByRef lp_strExportFileName As String)
    
    Screen.MousePointer = vbHourglass
    
    Dim crxApplication As CRAXDRT.Application
    Dim crxReport As CRAXDRT.Report
    
    Set crxApplication = New CRAXDRT.Application
    Set crxReport = crxApplication.OpenReport(GetUNCNameNT(lp_strRPTFileName))
    
    If Len(lp_strSelectionFormula) > 0 Then
        crxReport.RecordSelectionFormula = lp_strSelectionFormula
    End If
    
    crxReport.DiscardSavedData
    
    crxReport.ExportOptions.DestinationType = crEDTDiskFile
    crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
    crxReport.ExportOptions.DiskFileName = lp_strExportFileName
    crxReport.ExportOptions.ExcelUseConstantColumnWidth = False
    crxReport.ExportOptions.ExcelUseFormatInDataOnly = True
    crxReport.Export False

    'Dim l_intLoop  As Integer
    'For l_intLoop = 0 To 10000
    '    DoEvents
    'Next
     
    Set crxReport = Nothing
    crxApplication.CanClose
    Set crxApplication = Nothing
    
    DoEvents
    
    Screen.MousePointer = vbDefault

End Sub

Public Function CurrentMachineName() As String
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
End Function

' Private function that does the work under Windows NT
Public Function GetUNCNameNT(pathName As String) As String

Dim hKey As Long
Dim hKey2 As Long
Dim exitFlag As Boolean
Dim i As Double
Dim errCode As Long
Dim rootKey As String
Dim Key As String
Dim computerName As String
Dim lComputerName As Long
Dim stPath As String
Dim firstLoop As Boolean
Dim Ret As Boolean

' first, verify whether the disk is connected to the network
If Mid(pathName, 2, 1) = ":" Then
   Dim UNCName As String
   Dim lenUNC As Long

   UNCName = String$(520, 0)
   lenUNC = 520
   errCode = WNetGetConnection(Left(pathName, 2), UNCName, lenUNC)

   If errCode = 0 Then
      UNCName = Trim(Left$(UNCName, InStr(UNCName, _
        vbNullChar) - 1))
      GetUNCNameNT = UNCName & Mid(pathName, 3)
      Exit Function
   End If
End If

' else, scan the registry looking for shared resources
'(NT version)
computerName = String$(255, 0)
lComputerName = Len(computerName)
errCode = GetComputerName(computerName, lComputerName)
If errCode <> 1 Then
   GetUNCNameNT = pathName
   Exit Function
End If

computerName = Trim(Left$(computerName, InStr(computerName, _
   vbNullChar) - 1))
rootKey = "SYSTEM\CurrentControlSet\Services\LanmanServer\Shares"
errCode = RegOpenKey(HKEY_LOCAL_MACHINE, rootKey, hKey)

If errCode <> 0 Then
   GetUNCNameNT = pathName
   Exit Function
End If

firstLoop = True

Do Until exitFlag
   Dim szValue As String
   Dim szValueName As String
   Dim cchValueName As Long
   Dim dwValueType As Long
   Dim dwValueSize As Long

   szValueName = String(1024, 0)
   cchValueName = Len(szValueName)
   szValue = String$(500, 0)
   dwValueSize = Len(szValue)

   ' loop on "i" to access all shared DLLs
   ' szValueName will receive the key that identifies an element
   errCode = RegEnumValue(hKey, i#, szValueName, _
       cchValueName, 0, dwValueType, szValue, dwValueSize)

   If errCode <> 0 Then
      If Not firstLoop Then
         exitFlag = True
      Else
         i = -1
         firstLoop = False
      End If
   Else
      stPath = GetPath(szValue)
      If firstLoop Then
         Ret = (UCase(stPath) = UCase(pathName))
         stPath = ""
      Else
         Ret = (UCase(stPath) = UCase(Left$(pathName, _
        Len(stPath))))
         stPath = Mid$(pathName, Len(stPath))
      End If
      If Ret Then
         exitFlag = True
         szValueName = Left$(szValueName, cchValueName)
         GetUNCNameNT = "\\" & computerName & "\" & _
            szValueName & stPath
      End If
   End If
   i = i + 1
Loop

RegCloseKey hKey
If GetUNCNameNT = "" Then GetUNCNameNT = pathName

End Function

' support routine
Private Function GetPath(st As String) As String
   Dim pos1 As Long, pos2 As Long, pos3 As Long
   Dim stPath As String

   pos1 = InStr(st, "Path")
   If pos1 > 0 Then
      pos2 = InStr(pos1, st, vbNullChar)
      stPath = Mid$(st, pos1, pos2 - pos1)
      pos3 = InStr(stPath, "=")
      If pos3 > 0 Then
         stPath = Mid$(stPath, pos3 + 1)
         GetPath = stPath
      End If
   End If
End Function

Function GetFlag(lp_intFlagValue As Variant)
    
    Select Case lp_intFlagValue
        Case 1, -1, True, "TRUE", "True", "true", "YES", "Yes", "yes"
            GetFlag = 1
        Case 0, "FALSE", "False", "false", "no", "NO", "No"
            GetFlag = 0
        Case Else
            GetFlag = 0
    End Select
    
End Function

Sub NotifyByEMail(lp_strBatchReport As String)

Dim rs As ADODB.Recordset, SQL As String

SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'MX1' and trackermessageID = 28;"
Set rs = New ADODB.Recordset

rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    rs.MoveFirst
    While Not rs.EOF
        SendSMTPMail rs("email"), rs("fullname"), "MX1 Posting to Accounts", "A Posting Batch has been sent to Accounts.", lp_strBatchReport, True, "", "", g_strAdministratorEmailAddress
        rs.MoveNext
    Wend
End If
rs.Close
Set rs = Nothing

End Sub

Sub NotifyProblemByEmail(lp_lngJobID As Long, lp_strMessage As String)

Dim rs As ADODB.Recordset, SQL As String

SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'MX1' and trackermessageID = 28;"
Set rs = New ADODB.Recordset

rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    rs.MoveFirst
    Do While Not rs.EOF
        SendSMTPMail rs("email"), rs("fullname"), "MX1 Posting to Priority - Issue", lp_lngJobID & " will not be posted:" & vbCrLf & lp_strMessage, "", True, "", "", g_strAdministratorEmailAddress
        rs.MoveNext
    Loop
End If
rs.Close
Set rs = Nothing

End Sub

Sub ResetSentToAccountsOnJob(lp_lngJobID As Long, lp_strType As String)
    
Select Case lp_strType
    Case "INVOICE"
        SetData "job", "senttoaccountsdate", "jobID", lp_lngJobID, Null
        SetData "job", "senttoaccountsuser", "jobID", lp_lngJobID, Null
    Case "CREDIT"
        Dim l_lngCreditNoteNumber As Long
        l_lngCreditNoteNumber = GetData("job", "creditnotenumber", "jobID", lp_lngJobID)
        SetData "invoiceheader", "senttoaccountsdate", "creditnotenumber", l_lngCreditNoteNumber, Null
    Case Else
        SetData "job", "senttoBBCdate", "jobID", lp_lngJobID, Null
End Select
        
End Sub

Function GetCount(lp_strSQL As String) As Double

Screen.MousePointer = vbHourglass

Dim l_conSearch As ADODB.Connection
Dim l_rstSearch As ADODB.Recordset

Set l_conSearch = New ADODB.Connection
Set l_rstSearch = New ADODB.Recordset

l_conSearch.ConnectionString = g_strConnection
l_conSearch.Open

With l_rstSearch
     .CursorLocation = adUseClient
     .LockType = adLockBatchOptimistic
     .CursorType = adOpenDynamic
     .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
End With

l_rstSearch.ActiveConnection = Nothing

If l_rstSearch.EOF Then
    GetCount = 0
Else

    If Not IsNull(l_rstSearch(0)) Then
        GetCount = Val(l_rstSearch(0))
    Else
        GetCount = 0
    End If
End If

l_rstSearch.Close
Set l_rstSearch = Nothing

l_conSearch.Close
Set l_conSearch = Nothing

Screen.MousePointer = vbDefault

End Function



